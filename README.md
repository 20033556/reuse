# ReUse
## Requisiti
- [x] Prodotto in vendita
> - [x] Campi
>> - [x] Nome
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] categoria di appartenenza
>>> - [x] Get
>>> - [x] Put
>>> - [x] Post
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] sotto-categoria di appartenenza
>>> - [x] Get
>>> - [x] Put
>>> - [x] Post
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] localizzazione geografica
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] descrizione
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] prezzo
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] stato dell'oggetto (menù a tendina con 4 categorie: nuovo, usato ma in perfette condizioni, qualche lieve difetto, difettoso)
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] una o più foto
>>> - [x] Creazione
>>> - [x] Mostra a schermo
>> - [x] informazioni del proprietario
>>> - [x] email,
>>>> - [x] Creazione
>>>> - [x] Mostra a schermo
>>> - [x] numero di telefono)
>>>> - [x] Creazione
>>>> - [x] Mostra a schermo
>> - [x] venduto
>>> - [x] Creazione
>>> - [x] Mostra a schermo
> - [x] Firestore
>> - [x] Get
>> - [x] Put
>> - [x] Post
>> - [x] Delete

- [x] Utente
> - [x] Registrazione
> - [x] Login
> - [x] Annunci
>> - [x] Get
>>> - [x] All
>>> - [x] By filters
>>>> - [x] Name
>>>> - [x] range (il centro e' la posizione dell'utente)
>>>> - [x] price (RANGE or upper/lower bound
>>>> - [x] available for shipping 
>>>> - [x] Salvare ricerca
>>>>> - [x] Ricevere notifiche quando un Prodotto viene aggiunto alla ricerca
>> - [x] Put (solo se il proprio)
>> - [x] Post
>> - [x] Delete (solo se il proprio)
>> - [x] Chat con altro venditore
>>> - [x] ?? Notifica di messaggio
>>> - [x] Chiusura dell'annuncio (venduto = true)
> - [x] _Budget_
>> - [x] _Aumenta quando vende un Prodotto_
>> - [x] _Aumenta con carta di credito_
>> - [x] _Diminuisce quando compra un Prodotto_
>>> - [x] ?? _Check budget >= prezzo_
> - [x] _Quando prodotto venduto, lascia votazione all'altro Utente_
>> - [x] _Rating 1..5_
>> - [x] _Breve recensione_

- [x] Admin
> - [x] Modifica ogni campo del Prodotto
>> - [x] Nome
>> - [x] categoria di appartenenza
>> - [x] sotto-categoria di appartenenza
>> - [x] localizzazione geografica
>> - [x] descrizione
>> - [x] prezzo
>> - [x] stato dell'oggetto
>> - [x] una o più foto
> - [x] Operazioni su categorie
>> - [x] Put
>> - [x] Post
> - [x] Operazioni su sotto-categorie
>> - [x] Put
>> - [x] Post
> - [x] _Chat con utenti_
> - [x] _Freeze utenti_
> - [x] _Delete utenti_
> - [x] _Statistiche_
>> - [x] _Oggetti in vendita in totale_
>> - [x] _Oggetti in vendita per specifico utente_
>>> - [x] _Ricerca Prodotti utente_
>> - [x] _Tempo medio di vendita (giorni)_
>> - [x] _Classifica utenti con voto piu' alto (rating)_
>> - [x] _Numero di oggetti in vendita per provincia (scrive il nome della provincia e si torna un count query)_

## Statistiche
| Completati | Totali | Valore x1 | Percentuale |
| - | - | - | - |
| 100 | 100 | 1% | 100% |

####################################################################################################

