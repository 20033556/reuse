package com.example.reuse.chat

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.firebase.Snapshot
import com.example.reuse.obj.Product
import com.example.reuse.obj.User


class ChatList : Fragment(R.layout.fragment_chat_list) {

    private lateinit var loggedUser: User

    private val activeChats: ArrayList<Pair<Product,User>> = arrayListOf()
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ChatListAdapter
    private lateinit var navController: NavController

    private lateinit var refreshBtn: AppCompatButton
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.chatListCardContainer)
        navController = findNavController()
        recyclerView.layoutManager = LinearLayoutManager(context)
        Callout.getUser(requireContext(),::setUser)

        refreshBtn = view.findViewById(R.id.refreshChatList)
        refreshBtn.setOnClickListener {
            it.isEnabled = false
            Snapshot.getUserChats(loggedUser,context,::showChats)
            showMessage("Updating...",context, tagInput = tag)
        }
    }

    private fun setUser(user: User){
        loggedUser = user
        adapter = ChatListAdapter(context,activeChats,user,navController)
        recyclerView.adapter = adapter
        Snapshot.getUserChats(loggedUser,context,::showChats)
    }

    private fun showChats(chatPairs: Pair<Product,User>){
       if(!activeChats.contains(chatPairs)){
           activeChats.add(chatPairs)
           adapter.notifyItemInserted(activeChats.size)
       }
        refreshBtn.isEnabled = true
    }
}

private class ChatListAdapter (
    context: Context?,
    private val chatList: ArrayList<Pair<Product,User>>,
    private val loggedUser: User,
    private val navController: NavController
) : RecyclerView.Adapter<ChatListAdapter.RecyclerHolder>() {


    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameView: TextView = itemView.findViewById(R.id.chatItemProductName)
        val chatWithView: TextView = itemView.findViewById(R.id.chatItemUser)
    }

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
        //for each item, set productNameView to the name of the product
        val product: Product = chatList[position].first
        val otherUser: User = chatList[position].second
        holder.productNameView.text = product.name
        holder.chatWithView.text = otherUser.email
        holder.itemView.setOnClickListener {
            navController.navigate(ChatListDirections.actionChatListToChatPage(loggedUser, product, otherUser.email))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = mInflater.inflate(R.layout.chat_item_view, parent, false)
        return RecyclerHolder(view)
    }

    override fun getItemCount() = chatList.size
}