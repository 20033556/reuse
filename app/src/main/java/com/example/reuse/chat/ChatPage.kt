package com.example.reuse.chat

import android.app.NotificationManager
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.firebase.Snapshot
import com.example.reuse.obj.Product
import com.example.reuse.obj.User

class ChatPage : Fragment(R.layout.fragment_chat_page) {

    private val args: ChatPageArgs by navArgs()
    private lateinit var product: Product
    private lateinit var otherUser: String
    private lateinit var loggedUser: User

    private lateinit var messageEditText: EditText
    private lateinit var ownerMail: String
    private lateinit var buyerMail: String

    private var messageList: ArrayList<String> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ChatPageAdapter

    private lateinit var sendBtn: AppCompatButton

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        product = args.product
        otherUser = args.otherUser
        loggedUser = args.loggedUser

        ownerMail = product.ownerMail
        buyerMail = if(ownerMail == loggedUser.email) otherUser else loggedUser.email

        recyclerView = requireView().findViewById(R.id.chatCardContainer)
        adapter = ChatPageAdapter(context,messageList,loggedUser.email)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter


        messageEditText = view.findViewById(R.id.createMessage)
        //send message logic
        sendBtn = view.findViewById(R.id.sendMessageBtn)
        listenForSend()

        //text at the top
        view.findViewById<TextView>(R.id.chatWithWho).text = otherUser

        //chat messages
        val notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        Snapshot.getOlderMessages(product.id,ownerMail,buyerMail, requireContext(),notificationManager, ::showMessage)
    }

    private fun listenForSend(){
        //arrow button
        sendBtn.setOnClickListener {
            readAndSendMessage()
        }
        //return key
        messageEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                readAndSendMessage()
                return@setOnEditorActionListener true
            }
            false
        }

    }
    private fun readAndSendMessage(){
        sendBtn.isEnabled = false
        val message: String = messageEditText.text.toString()

        if(message.isBlank()) {
            sendBtn.isEnabled = true
            return
        }
        Snapshot.postRealtimeMessage(
            sanitizeMessage(message, loggedUser.email)[0],
            product.id,
            ownerMail,
            buyerMail,
            requireContext(),
            requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager,
            fun(){
                messageEditText.text.clear()
                sendBtn.isEnabled = true
            }
        )
    }

    private fun showMessage(message: String){
        messageList.add(message)
        adapter.notifyItemInserted(messageList.size)
        recyclerView.layoutManager!!.scrollToPosition(adapter.itemCount-1)
    }

    companion object{
        private const val separator = "#?!@%"

        fun sanitizeMessage(message: String, loggedUserMail: String? = null): List<String>{
            //if loggedUserMail != null, i'm sending the message
            return if(loggedUserMail != null)
                    listOf("$message$separator${loggedUserMail}")
                else message.split(separator)
        }
    }
}

private class ChatPageAdapter(context: Context?, private val messages: ArrayList<String>, private val userMail: String): RecyclerView.Adapter<ChatPageAdapter.RecyclerHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val messageView: TextView = itemView.findViewById(R.id.chatMessage)
        val youView: TextView = itemView.findViewById(R.id.chatMessageOwner)
    }

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
        val split = ChatPage.sanitizeMessage(messages[position])
        holder.messageView.text = split[0]
        val messageOwner = split[1]
        if(messageOwner == userMail){
            //the logged user is the owner of the message
            holder.youView.visibility = View.VISIBLE
            holder.messageView.typeface = Typeface.DEFAULT
            holder.messageView.textAlignment = View.TEXT_ALIGNMENT_VIEW_END
        } else {
            //if the message is from the other user, make it bold and move it the left
            holder.youView.visibility = View.GONE
            holder.messageView.typeface = Typeface.DEFAULT_BOLD
            holder.messageView.textAlignment = View.TEXT_ALIGNMENT_VIEW_START
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = mInflater.inflate(R.layout.message_item_view, parent, false)
        return RecyclerHolder(view)
    }

    override fun getItemCount() = messages.size
}