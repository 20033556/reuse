package com.example.reuse

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.reuse.firebase.Callout
import com.example.reuse.login.Login
import com.example.reuse.login.LoginDirections
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var popStackButton: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // realtime database
        if(intent.getBooleanExtra("next",true)){
            //this makes sure we only call this function once in the application lifetime
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        }

        bottomNavigationView = findViewById(R.id.bnv_home)
        popStackButton = findViewById(R.id.cancelBtn)
        navController = findNavController(R.id.nav_host_fragment)
        popStackButton.setOnClickListener {
            it.isEnabled = false
            navController.popBackStack()
            it.isEnabled = true
        }
        bottomNavigationView.setupWithNavController(navController)

        if (Firebase.auth.currentUser != null) {

            // make sure the user is on the homepage if he's already authenticated
            val navFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
            val currentFragment = navFragment!!.childFragmentManager.primaryNavigationFragment

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            Callout.getUser(this,fun(user){

                if(user.isActive){
                    if (currentFragment is Login)
                        navController.navigate(LoginDirections.actionLoginToSearchPage())
                } else {
                    navController.navigate(LoginDirections.actionLoginToBanPage())
                    return
                }
            },notificationManager)

        }

        navController.addOnDestinationChangedListener { _, destination: NavDestination, _ ->
            checkIfDestinationNeedNavbar(destination)
        }


    }

    private fun checkIfDestinationNeedNavbar(destination: NavDestination) {
        bottomNavigationView.visibility =
            if(destination.id == R.id.banPage || destination.id == R.id.login)
                View.GONE
            else View.VISIBLE
        popStackButton.visibility = when (destination.id){
            R.id.banPage, R.id.login, R.id.searchPage, R.id.adminPanel, R.id.personalPage, R.id.my_products, R.id.chatList -> View.GONE
            else -> View.VISIBLE
        }
    }
}