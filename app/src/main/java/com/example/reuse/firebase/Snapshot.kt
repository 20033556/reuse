package com.example.reuse.firebase

import android.app.NotificationManager
import android.content.Context
import com.example.reuse.R
import com.example.reuse.Utility.Companion.filterProductsByQueryArgsForSnapshotBecauseWeDoNotCheatButHaveANecessity
import com.example.reuse.Utility.Companion.makeNotification
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.chat.ChatPage
import com.example.reuse.obj.Product
import com.example.reuse.obj.QueryArgs
import com.example.reuse.obj.User
import com.example.reuse.toPair
import com.example.reuse.toProduct
import com.google.firebase.FirebaseException
import com.google.firebase.database.*
import com.google.firebase.firestore.*
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

private const val tag = "Snapshot"
class Snapshot {
    companion object{
        private val referenceToListener: MutableMap<DatabaseReference,ChildEventListener?> = mutableMapOf()
        private val referenceToString: MutableMap<DatabaseReference, String> = mutableMapOf()
        private val referenceToChatDB: MutableMap<DatabaseReference, String> = mutableMapOf()
        private val chatidToListener: MutableMap<String, ListenerRegistration> = mutableMapOf()
        private val referenceToCount: MutableMap<DatabaseReference, Int> = mutableMapOf()
        private val referenceToCurrentCount: MutableMap<DatabaseReference, Int> = mutableMapOf()
        const val chatCollectionName = "Chats"
        private var firebaseChatDocumentId: String? = null

        private fun getDatabaseReference(chat: String): DatabaseReference{
            return FirebaseDatabase.getInstance().getReference(chat)
        }
        private fun getChatReferenceString(productId: String, ownerMail: String, userMail: String): String{
            val id = sanitizeForFirebase(productId)
            val own = sanitizeForFirebase(ownerMail)
            val email = sanitizeForFirebase(userMail)
            return "$id-$own-$email"
        }
        private fun sanitizeForFirebase(string: String): String{
            return string
                .replace(".","")
                .replace("#","")
                .replace("$","")
                .replace("[","")
                .replace("]","")
                .replace(" ","")
        }
        private fun getUserQueries(user: User): Array<Query>{
            val result: ArrayList<Query> = arrayListOf()
            val baseCollection = Firebase.firestore.collection(chatCollectionName)
            result.add(baseCollection.whereEqualTo("owner",user.email))
            result.add(baseCollection.whereEqualTo("user",user.email))
            return result.toTypedArray()
        }
        private fun getChatMap(productId: String, ownerMail: String, buyerMail: String, lastMessage: String? = null): MutableMap<String, Any?> {
            val map: MutableMap<String, Any?> = mutableMapOf()
            map["product"] = productId
            map["owner"] = ownerMail
            map["user"] = buyerMail
            val chat = getChatReferenceString(productId,ownerMail,buyerMail)
            map["chat"] = chat
            map["lastMessage"] = lastMessage
            map["messageCount"] = 0
            val dbRef = getDatabaseReference(chat)
            referenceToCount[dbRef] = 0
            return map
        }

        fun listenToActiveChats(loggedUser: User, context: Context, notificationManager: NotificationManager){
            //first called when the user logs in from callout
            //find chats where I'm the owner or the buyer
            val queries: Array<Query> = getUserQueries(loggedUser)
            for(query in queries){
                query.get().addOnCompleteListener {
                    if(!it.isSuccessful){
                        showMessage("Error finding chats",context,it.exception, tagInput = tag)
                        return@addOnCompleteListener
                    }
                    it.result.documents.distinct().forEach { doc: DocumentSnapshot ->
                        val chat: String = doc.data!!["chat"].toString()
                        showMessage("cicci $chat")
                        val ref = getDatabaseReference(chat)
                        if(chatidToListener[doc.id] != null)
                            return@addOnCompleteListener
                        referenceToString[ref] = chat
                        referenceToChatDB[ref] = doc.id
                        referenceToCount[ref] = doc.data!!["messageCount"].toString().toInt()
                        referenceToCurrentCount[ref] = 0
                        addSnapshotToChat(chat,loggedUser.email,chat,ref,context,notificationManager,fun(){})
                    }
                }
            }
        }

        fun getOlderMessages(productId: String, ownerMail: String, userMail: String, context: Context,notificationManager: NotificationManager, callback: (String) -> Unit){
            val chatString = getChatReferenceString(productId, ownerMail, userMail)
            getOlderMessages(chatString,userMail,context, notificationManager,callback)
        }
        private fun getOlderMessages(chatString: String,userMail: String, context: Context, notificationManager: NotificationManager, callback: (String) -> Unit){
            val databaseReference: DatabaseReference = getDatabaseReference(chatString)
            val oldListener: ChildEventListener? = referenceToListener[databaseReference]
            if(oldListener != null){
                //remove older listener from the dbRef
                databaseReference.removeEventListener(oldListener)
                referenceToCurrentCount[databaseReference] = 0
                showMessage("restarted listener to chat",context, tagInput = tag)
            }
            referenceToListener[databaseReference] = databaseReference.addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                    // This method is called once for each initial child and then again
                    // for each new child added to the location.
                    val newMessage = dataSnapshot.value.toString()
                    if(referenceToCount[databaseReference]!! <= referenceToCurrentCount[databaseReference]!!){
                        // invio notifica
                        checkForSendChatNotification(newMessage,userMail,notificationManager,context,chatString)

                    }
                    referenceToCurrentCount[databaseReference] = referenceToCurrentCount[databaseReference]!! + 1
                    callback(newMessage)
                }
                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    showMessage("Error getting new message",context,error.toException(),tag)
                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildRemoved(snapshot: DataSnapshot) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            })
            referenceToString[databaseReference] = chatString
        }
        fun postRealtimeMessage(message: String, productId: String, ownerMail: String, userMail: String, context: Context, notificationManager: NotificationManager, callback: () -> Unit){
            val chatString = getChatReferenceString(productId, ownerMail, userMail)
            postRealtimeMessage(message,chatString,userMail,context,notificationManager,callback)
        }
        private fun postRealtimeMessage(message: String, chatString: String, userMail: String,context: Context, notificationManager: NotificationManager, callback: () -> Unit) {
            val databaseReference = getDatabaseReference(chatString)
            //send message to realtime db
            databaseReference.push().setValue(message).addOnCompleteListener { task0 ->
                if(!task0.isSuccessful){
                    showMessage("Error sending message.",context,task0.exception, tagInput = tag)
                    return@addOnCompleteListener
                }
                //the message was sent
                val chatId: String? = if(firebaseChatDocumentId == null) referenceToChatDB[databaseReference] else firebaseChatDocumentId
                firebaseChatDocumentId = chatId
                val messageCount = referenceToCurrentCount[databaseReference]!!

                //add the message and the count to the chat document (the count gets updated by the other listener)
                Firebase.firestore.collection(chatCollectionName).document(chatId!!).update("lastMessage",message,"messageCount",messageCount).addOnCompleteListener { task1 ->
                    if(!task1.isSuccessful){
                        showMessage("Error uploading message",context,task1.exception, tag)
                        @Suppress("LABEL_NAME_CLASH")
                        return@addOnCompleteListener
                    }

                    addSnapshotToChat(chatId,userMail,chatString,databaseReference,context,notificationManager,callback)
                }
            }
        }
        private fun addSnapshotToChat(chatDocumentId: String,userMail: String,chatString: String,databaseReference:DatabaseReference,context: Context,notificationManager: NotificationManager, callback: () -> Unit){
            //the snapshot lives as long as the app is running. there's no real need to remove the listener
            if(chatidToListener[chatDocumentId] != null || referenceToListener[databaseReference] != null){
                callback()
                return
            }

            chatidToListener[chatDocumentId] = Firebase.firestore.collection(chatCollectionName).document(chatDocumentId).addSnapshotListener{ doc: DocumentSnapshot?, exc: FirebaseException? ->
                if(exc != null || doc == null){
                    showMessage("Error with snapshot of chat",context,exc, tag)
                    return@addSnapshotListener
                }
                if(doc.data == null) return@addSnapshotListener//nessun nuovo messaggio


                val lastMessage: String = doc.data!!["lastMessage"].toString()
                checkForSendChatNotification(lastMessage,userMail,notificationManager,context,chatString)
            }
            callback()
        }

        fun getUserChats(user: User, context: Context?, callback: (Pair<Product,User>) -> Unit) {
            val queries: Array<Query> = getUserQueries(user)
            for(query in queries){
                query.get().addOnCompleteListener { task ->
                    if(!task.isSuccessful){
                        showMessage("Error getting chats",context,task.exception, tagInput = tag)
                        return@addOnCompleteListener
                    }
                    val documents = task.result.documents
                    val result: List<Pair<Product,User>> = documents.map { doc: DocumentSnapshot -> doc.toPair(user) }
                    Callout.getProductById(result,callback)
                }
            }
        }

        fun postNewChat(productId: String, ownerMail: String, buyerMail: String, context: Context?){
            val newDoc = Firebase.firestore.collection(chatCollectionName).document()
            firebaseChatDocumentId = newDoc.id
            val map: MutableMap<String, Any?> = getChatMap(productId,ownerMail,buyerMail)
            newDoc.set(map).addOnCompleteListener { task ->
                if(!task.isSuccessful){
                    showMessage("Error adding chat details",context,task.exception, tag)
                    return@addOnCompleteListener
                }
                //no-op
                referenceToCurrentCount[getDatabaseReference(getChatReferenceString(productId,ownerMail,buyerMail))] = 0
            }
        }

        private fun checkForSendChatNotification(newMessage: String, userMail: String, notificationManager: NotificationManager, context: Context, chatString: String){
            val lastMessage: List<String> = ChatPage.sanitizeMessage(newMessage)
            if(lastMessage[1] != userMail){
                // send notification
                makeNotification(notificationManager,context,chatString,"New message",lastMessage[0],R.drawable.outline_campaign_24)
            }//else the last message was sent by the logged in user
        }
// chat
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// save search
        private val referenceToFavouritesListener: MutableMap<DocumentReference,ListenerRegistration> = mutableMapOf()

        fun snapshotProducts(queryArgs: QueryArgs, productCollectionName: String, context: Context, notificationManager: NotificationManager,callback: () -> Unit) {
            /*var firstRun = true
            //questa sarebbe la versione corretta. Il problema che si pone e' che lo snapshot viene lanciato
            //solo sui prodotti gia' esistenti nella query (quindi un nuovo prodotto non viene considerato)
            query.addSnapshotListener{ doc: QuerySnapshot?, exc: FirebaseException? ->
                if(exc != null || doc == null){
                    showMessage("Error with snapshot of chat",context,exc, tag)
                    return@addSnapshotListener
                }
                if(firstRun){
                    firstRun = false
                    return@addSnapshotListener
                }
                val changes = doc.documentChanges
                changes.forEach { documentChange: DocumentChange -> showMessage(documentChange.document.toString(), tagInput = "SaveSearch$tag") }
                //makeNotification(notificationManager,context,queryArgs.toString(),"New message",lastMessage)
            }*/
            var firstRun = true
            Firebase.firestore.collection(productCollectionName).addSnapshotListener{ doc: QuerySnapshot?, exc: FirebaseException? ->
                if(exc != null || doc == null){
                    showMessage("Error with snapshot of chat",context,exc, tag)
                    return@addSnapshotListener
                }
                if(firstRun){
                    firstRun = false
                    callback()
                    return@addSnapshotListener
                }
                val products: Array<Product> = doc.documentChanges.map { documentChange: DocumentChange -> documentChange.document.toProduct() }.toTypedArray()
                val filtered = filterProductsByQueryArgsForSnapshotBecauseWeDoNotCheatButHaveANecessity(products,queryArgs)
                if(filtered.isNotEmpty()){
                    // show notification
                    filtered.forEach {
                        makeNotification(notificationManager,context,"Saved search","New product","${it.name} is a NEW match!",
                        R.drawable.outline_fiber_new_24)
                    }
                }
            }
        }
        fun listenToFavourites(favProductIds: List<String>, context: Context, notificationManager: NotificationManager) {
            var firstRun = true
            favProductIds.forEach {
                val firestoreReference = Firebase.firestore.collection(Callout.productCollectionName).document(it)
                val listener = firestoreReference.addSnapshotListener{ doc: DocumentSnapshot?, exc: FirebaseException? ->
                    if(exc != null || doc == null){
                        showMessage("Error with snapshot of chat",context,exc, tag)
                        return@addSnapshotListener
                    }
                    if(firstRun){
                        firstRun = false
                        return@addSnapshotListener
                    }
                    val product = doc.toProduct()
                    makeNotification(notificationManager,context,"Favourites","New update","${product.name} was updated!",R.drawable.outline_announcement_24)
                }
                referenceToFavouritesListener[firestoreReference] = listener //listener.remove()
            }
        }

    }
}
