package com.example.reuse.firebase

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import com.example.reuse.*
import com.example.reuse.Utility.Companion.filterProductsByQueryArgs
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.obj.Product
import com.example.reuse.obj.QueryArgs
import com.example.reuse.obj.User
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.time.LocalDate
import java.time.Period

private const val tag = "Callout"
class Callout {
    companion object {
        private var user: User? = null
        private const val userCollectionName = "Users"
        private val userCollection = Firebase.firestore.collection(userCollectionName)
        private var setSavedSearchesListeners = false
        private const val favouritesCollectionName = "Favourites"

        fun getUser(context: Context, callback : (User) -> Any, notificationManager: NotificationManager? = null) {
            initializeForCategories()
            if(user != null) callback(user!!)
            else if(notificationManager != null) getUserFromFirestore(context,notificationManager,callback)
            else showMessage("Didn't pass a notification manager")
        }
        fun signOut(context: Context, activity: Activity){
            Firebase.auth.signOut()
            this.user = null
            context.cacheDir.deleteRecursively()
            val intent = Intent(activity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags += Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("next",false)
            activity.finishAffinity()
            activity.startActivity(intent)
            activity.finish()
        }

        private fun getUserFromFirestore(context: Context, notificationManager: NotificationManager, callback: (User) -> Any) {
            val userMail: String? = FirebaseAuth.getInstance().currentUser?.email
            if(userMail == null){
                showMessage("Current user mail is null",context, tagInput = tag)
                return
            }

            userCollection.document(userMail).get().addOnCompleteListener {
                if (!it.isSuccessful) {
                    showMessage("\${R.string.errGetDocs} (3).",context,it.exception, tag)
                    //TODO("alfredo vedi se si puo' fare questo sopra")
                    return@addOnCompleteListener
                }
                val document = it.result
                if (document == null) {
                    showMessage("Error finding user's document", context, tagInput = tag)
                    return@addOnCompleteListener
                }
                val thisuser: User = document.toUser()
                user = thisuser
                setupBackgroundListener(thisuser,context,notificationManager,callback)
            }
        }
        private fun setupBackgroundListener(user: User, context: Context, notificationManager: NotificationManager, callback: (User) -> Any) {
            Snapshot.listenToActiveChats(user,context,notificationManager)
            if(!setSavedSearchesListeners)
                getAndSnapToSavedSearches(user,context,notificationManager)
            snapToFavourites(user,context,notificationManager)
            callback(user)
        }

        fun postUser(user: User,context: Context?){
            initializeForCategories()
            userCollection.document(user.email).set(user).addOnCompleteListener {
                if (it.isSuccessful) {
                    showMessage("User saved",context, tagInput = tag)
                    this.user = user
                } else showMessage("Error saving user",context,it.exception, tag)
            }
        }

        fun deleteUserRelatedData(user: User, password: String, context: Context, activity: Activity, callback : () -> Unit, errorCallback: () -> Unit){
            showMessage("Deleting...",context)
            deleteUserAuth(user,password,context,fun(){
                signOut(context,activity)
                callback()
            },errorCallback)
        }
        private fun deleteUserAuth(user: User, password: String, context: Context, callback: () -> Unit, errorCallback: () -> Unit){
            val credential = EmailAuthProvider.getCredential(user.email, password)
            Firebase.auth.currentUser!!.reauthenticate(credential).addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Invalid password.",context,it.exception, tag)
                    errorCallback()
                    return@addOnCompleteListener
                }
                val fuser = Firebase.auth.currentUser!!

                Firebase.auth.signOut()
                fuser.delete().addOnCompleteListener { task ->
                    if (!task.isSuccessful){
                        showMessage("Error deleting user auth",context,task.exception, tag)
                        @Suppress("LABEL_NAME_CLASH")
                        return@addOnCompleteListener
                    }
                    showMessage("deleted from auth")
                    deleteUserFromFirestore(user,context,callback)
                }
            }
        }

        private fun deleteUserFromFirestore(user: User, context: Context, callback: () -> Unit) {
            val relevantPath = userCollection.document(user.email)
            relevantPath.delete().addOnCompleteListener { task0 ->
                if(!task0.isSuccessful) {
                    showMessage("Error deleting user records.",context,task0.exception, tag)
                    return@addOnCompleteListener
                }
                //also delete subcollections (queries, favourites, reviews)
                //queries
                getAndSnapToSavedSearches(user,context,callback = fun(queryDoc: List<DocumentSnapshot>){
                    queryDoc.forEach { relevantPath.delete().addOnFailureListener{ _ -> showMessage("Error deleting query ${it.id}",context, tagInput = tag) }}
                    //favourites
                    getFavourites(user,context,fun(products: List<String>){
                        products.forEach { p ->
                            relevantPath.collection(favouritesCollectionName).document(p).delete().addOnFailureListener { showMessage("Error deleting favourite $p",context, tagInput = tag) }
                        }
                    })
                    //reviews
                    getUserReviews(context,user.email,fun(doc: List<DocumentSnapshot>){
                        doc.forEach { relevantPath.collection(reviewCollectionName).document(it.id).delete().addOnFailureListener{ _ -> showMessage("Error deleting review ${it.id}",context, tagInput = tag) } }
                    })
                    deleteUserChats(user,context,callback)
                })
            }
        }
        private fun deleteUserChats(user: User, context: Context?,  callback: () -> Unit) {
            showMessage("deleted from firestore")
            Firebase.firestore.collection(Snapshot.chatCollectionName).whereEqualTo("owner",user.email).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error finding user's products",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                it.result.documents.forEach {el->
                    Firebase.firestore.collection(Snapshot.chatCollectionName).document(el.id).delete().addOnCompleteListener{res->
                        if(!res.isSuccessful){
                            showMessage("Error finding user's products",context,it.exception,tag)
                            @Suppress("LABEL_NAME_CLASH")
                            return@addOnCompleteListener
                        }
                    }
                }
                showMessage("deleted chats")
                deleteUserProducts(user,context,callback)
            }
       }
        private fun deleteUserProducts(user: User, context: Context?, callback: () -> Unit){
            productCollection.whereEqualTo("ownerMail",user.email).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error finding user's products",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                val documents = it.result.documents
                documents.forEach { doc ->
                    productCollection.document(doc.id).delete().addOnCompleteListener{ del ->
                        if(!del.isSuccessful) showMessage("Error deleting document ${doc.id}",context,del.exception,tag)
                    }
                }
                showMessage("deletd products")
                callback()
            }
        }

        fun putUserBudget(user: User, delta: Float, context: Context?, callback: () -> Unit){
            val newBudget = user.budget + delta
            userCollection.document(user.email).update("budget", newBudget).addOnCompleteListener {
                if(it.isSuccessful){
                    if(this.user!! == user){
                        this.user!!.budget = newBudget
                    }
                    callback()
                } else {
                    showMessage("Error updating user's budget.",context,it.exception,tag)
                }
            }
        }

        private const val savedQueriesColletionName = "Queries"
        fun postSearch(user: User, queryArgs: QueryArgs, context: Context, notificationManager: NotificationManager, callback: () -> Unit) {
            userCollection.document(user.email).collection(savedQueriesColletionName).document().set(queryArgs).addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error saving search",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                //add snapshot
                Snapshot.snapshotProducts(queryArgs,productCollectionName,context,notificationManager,callback)
            }
        }

        private fun getAndSnapToSavedSearches(user: User, context: Context, notificationManager: NotificationManager? = null, callback: ((List<DocumentSnapshot>) -> Unit)? = null){
            //get saved searches
            userCollection.document(user.email).collection(savedQueriesColletionName).get().addOnCompleteListener { task ->
                if(!task.isSuccessful){
                    showMessage("Error getting saved searches",context,task.exception, tag)
                    return@addOnCompleteListener
                }
                //because if 2 or more queryArgs are the same and if 2 or more are different, we get a notification
                //anyway when a product matches, we cat remove all reduntant queryArgs
                if(notificationManager != null) {
                    val queryArrrgs: List<QueryArgs> = ArrayList(task.result.documents.map { it.toQueryArgs() }).distinct()
                    queryArrrgs.forEach {
                        Snapshot.snapshotProducts(
                            it,
                            productCollectionName,
                            context,
                            notificationManager,
                            fun() {})
                    }
                    setSavedSearchesListeners = true
                }
                if(callback != null){
                    callback(task.result.documents)
                }
            }
        }

        fun getFavourites(user:User, context:Context, callback:(List<String>) -> Unit){
            getFavourites(context,user,fun(productIdsDocs: List<DocumentSnapshot>){
                val productsIds = productIdsDocs.map { it.id }
                showMessage(productsIds.toString(), tagInput = "alpha")
                callback(productsIds)
            })
        }
        private fun getFavourites(context: Context, user: User, callback: (List<DocumentSnapshot>) -> Unit){
            userCollection.document(user.email).collection(favouritesCollectionName).get().addOnCompleteListener {
                if (!it.isSuccessful) {
                    showMessage("Error finding favourites", context, it.exception, tag)
                    return@addOnCompleteListener
                }
                showMessage(it.result.documents.toString(), tagInput = "alpha")
                callback(it.result.documents)
            }
        }
        private fun snapToFavourites(user: User, context: Context, notificationManager: NotificationManager){
            getFavourites(user,context,fun(productsIds: List<String>){
                Snapshot.listenToFavourites(productsIds,context,notificationManager)
            })
        }

        fun postFavourite(user: User, product: Product, context: Context, notificationManager: NotificationManager, callback: () -> Unit){
            userCollection.document(user.email).collection(favouritesCollectionName).document(product.id).set(mapOf("productId" to product.id)).addOnCompleteListener {
                if(!it.isSuccessful) {
                    showMessage("Error setting favourite",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                snapToFavourites(user,context,notificationManager)
                callback()
            }
        }
        fun deleteFavourite(user: User, product: Product, context: Context?, callback: () -> Unit){
            userCollection.document(user.email).collection(favouritesCollectionName).document(product.id).delete().addOnCompleteListener {
                if(!it.isSuccessful) showMessage("Error deleting favourite",context,it.exception,tag)
                else callback()
            }
        }

//user
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//products
        const val productCollectionName = "Products"
        private val productCollection = Firebase.firestore.collection(productCollectionName)
        fun postProduct(product: Product, context: Context?, callback: (String) -> Unit){
            val newDocument = productCollection.document()
            product.id = newDocument.id
            newDocument.set(product.toMap()).addOnCompleteListener {
                if (it.isSuccessful) callback(product.id)
                else showMessage("Error saving product",context,it.exception, tag)
            }
        }

        fun getMyProducts(context: Context?, callback: (Array<Product>) -> Unit){
            getProductsByFilters(QueryArgs(email = user!!.email),context,fun(products, _){
                callback(products)
            })
        }
        fun getProductsByFilters(queryArgs: QueryArgs, context: Context?, callback: (Array<Product>, QueryArgs) -> Unit) {
            //get all documents in productCollection then search in the myProducts collection with where clauses
            val query: Query = createQueryFromArgs(queryArgs)
            //get all relevant products then callback
            getProductsByQuery(query, queryArgs, context, callback)
        }
        private fun createQueryFromArgs(queryArgs: QueryArgs): Query {//also used by Snapshot class
            var query : Query = productCollection
            //if(queryArgs.name != null) query = query.whereEqualTo("name",queryArgs.name)
            /*if(queryArgs.range != null) {
                if(queryArgs.latitude == null || queryArgs.longitude == null){
                    showMessage("Cannot filter by range as center is missing",context, tagInput = tag)
                } else {
                    val center = GeoPoint(queryArgs.latitude!!,queryArgs.longitude!!)
                    val latDelta = queryArgs.range!! / 111300f
                    val lngDelta = queryArgs.range!! / (111300f * cos(center.latitude))
                    query = query
                        .whereGreaterThanOrEqualTo("location",geopointFromCoordinates(
                            center.latitude - latDelta,
                            center.longitude - lngDelta
                        ))
                        .whereLessThanOrEqualTo("location", geopointFromCoordinates(
                            center.latitude + latDelta,
                            center.longitude + lngDelta
                        ))
                }
            }*/
            if(queryArgs.priceSet){// && queryArgs.range == null) {
                //check min and max price
                if(queryArgs.minPrice != null)
                    query = query.whereGreaterThanOrEqualTo("price",queryArgs.minPrice!!)
                if(queryArgs.maxPrice != null)
                    query = query.whereLessThanOrEqualTo("price",queryArgs.maxPrice!!)
            }
            if(queryArgs.shippable != null)
                query = query.whereEqualTo("ship",queryArgs.shippable)
            if(queryArgs.email !=null)
                query = query.whereEqualTo("ownerMail",queryArgs.email)
            if(queryArgs.isSold !=null)
                query = query.whereEqualTo("isSold",queryArgs.isSold)

            return query
        }
        private fun getProductsByQuery(query: Query, queryArgs: QueryArgs, context: Context?, callback: (Array<Product>, QueryArgs) -> Unit){
            query.get().addOnCompleteListener{
                if(!it.isSuccessful || it.result == null) {
                    showMessage("Error getting filtered products.",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                val result = it.result
                if(result.isEmpty) {
                    callback(ArrayList<Product>().toTypedArray(), queryArgs)
                    return@addOnCompleteListener
                }
                val productsToFilter: List<Product> = (result.documents.map { doc: DocumentSnapshot -> doc.toProduct() })
                val products : Array<Product> = filterProductsByQueryArgs(productsToFilter,queryArgs)
                callback(products,queryArgs)
            }
        }

        fun putProduct(oldProduct: Product, newProduct: Product, context: Context?, callback: (Product) -> Unit){
            val toUpdate : MutableMap<String, Any> = getProductEdits(oldProduct,newProduct)
            productCollection.document(oldProduct.id)
                .update(toUpdate).addOnCompleteListener {
                    if(it.isSuccessful) callback(newProduct)
                    else showMessage("Error updating product.",context,it.exception,tag)
                }
        }
        private fun getProductEdits(oldProduct: Product, newProduct: Product): MutableMap<String, Any> {
            val changedMap : MutableMap<String, Any> = mutableMapOf()

            //if(oldProduct.id != newProduct.id) changedMap["id"] = newProduct.id
            if(oldProduct.name != newProduct.name) changedMap["name"] = newProduct.name
            if(oldProduct.category != newProduct.category) changedMap["category"] = newProduct.category
            if(oldProduct.subCategory != newProduct.subCategory) changedMap["subCategory"] = newProduct.subCategory!!
            if(oldProduct.latitude != newProduct.latitude || oldProduct.longitude != newProduct.longitude)
                changedMap["location"] = GeoPoint(newProduct.latitude, newProduct.longitude)
            if(oldProduct.description != newProduct.description) changedMap["description"] = newProduct.description
            if(oldProduct.price != newProduct.price) changedMap["price"] = newProduct.price
            if(oldProduct.status != newProduct.status) changedMap["status"] = newProduct.status
            if(oldProduct.ship != newProduct.ship) changedMap["ship"] = newProduct.ship
            //if(oldProduct.ownerMail != newProduct.ownerMail) changedMap["ownerMail"] = newProduct.ownerMail!!
            if(oldProduct.phone != newProduct.phone) changedMap["phone"] = newProduct.phone
            //if(oldProduct.creationDate != newProduct.creationDate) changedMap["creationDate"] = newProduct.creationDate
            if(oldProduct.province != newProduct.province) changedMap["province"] = newProduct.province!!
            //if(oldProduct.favourites != newProduct.favourites) changedMap["favourites"] = newProduct.favourites
            //if(oldProduct.photo != newProduct.photo) changedMap["photo"] = newProduct.photo
            if(oldProduct.isSold != newProduct.isSold) changedMap["isSold"] = newProduct.isSold

            return changedMap
        }

        fun deleteProduct(product: Product, context: Context?, callback: () -> Unit){
            productCollection.document(product.id).delete().addOnCompleteListener { task ->
                if(task.isSuccessful) callback()
                else showMessage("Error deleting product.",context,task.exception, tagInput = tag)
            }
        }

        fun getProductById(pairList: List<Pair<Product,User>>, callback: (Pair<Product, User>) -> Unit) {//used by Snapshot for the chatList
            pairList.forEach { pair: Pair<Product, User> ->
                productCollection.document(pair.first.id).get().addOnCompleteListener {
                    if(!it.isSuccessful){
                        showMessage("Error getting product name by id",exception=it.exception, tagInput = tag)
                        return@addOnCompleteListener
                    }
                    val product: Product = it.result.toProduct()
                    //could also retrieve the user (for now I only need the email so it's useless)
                    callback(Pair(product, pair.second))
                }
            }
        }
        fun getProductById(productId: String, context: Context?, callback: (Product) -> Unit){
            productCollection.document(productId).get().addOnCompleteListener {
                if(!it.isSuccessful) showMessage("Error getting product by id",context,it.exception,
                    tag)
                else callback(it.result.toProduct())
            }
        }

        fun buyProduct(product: Product, buyer: User, context: Context?, callback: () -> Unit) {
            if(buyer.budget < product.price){
                showMessage("Not enough funds",context, tagInput = tag)
                return
            }
            userCollection.document(product.ownerMail).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error getting owner data",context,it.exception, tag)
                    return@addOnCompleteListener
                }
                val owner: User = it.result.toUser()

                putUserBudget(buyer, - product.price,context,fun(){
                    putUserBudget(owner,product.price,context,fun(){
                        callback()
                    })
                })
            }
        }

        fun putProductToSold(product: Product, context: Context?, callback: () -> Unit){
            val sold = product.copy()
            sold.isSold = true
            //update sold field
            putProduct(product,sold,context,fun(_){
                // update statistics
                putProductStatistics(product,context,callback)
            })
        }
//products
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//category
        private const val categoriesCollectionName = "Categories"
        private const val subcategoriesCollectionName = "Subcategories"
        private const val categoryField = "category"
        private const val subCategoryField = "subcategories"
        private val categoriesCollection = Firebase.firestore.collection(categoriesCollectionName)
        private val subcategoriesCollection = Firebase.firestore.collection(
            subcategoriesCollectionName
        )
        private var categoryDocId: String? = null
        private var categoryDocument : DocumentReference? = null

        private fun initializeForCategories(){//this gets called when the user logs in or is registered
            if(categoryDocId != null && categoryDocument != null){
                return
            }
            categoriesCollection.limit(1).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error initializing for categories", exception = it.exception, tagInput = tag)
                    return@addOnCompleteListener
                }
                val doc = it.result.documents
                if(doc.isEmpty() || doc.size > 1){
                    showMessage("Error initializing for categories", tagInput = tag)
                    return@addOnCompleteListener
                }
                categoryDocId = doc[0].id
                categoryDocument = categoriesCollection.document(categoryDocId!!)
            }
        }

        private var countPost : Int? = null
        private fun normalizeCategoryStrings(name: String, index: Int? = null) : String {
            return if(index != null){
                //we have to add the index
                if (name.matches(Regex("^[a-zA-Z ]+-\\d+$")))
                    name
                else "$name-$index"
            } else {
                //we have to remove the index
                name.substringBefore("-")
            }
        }

        fun postCategorySubcategoriesBeginning(category: String, subcategories: Array<String>, context: Context?, callback: () -> Unit){
            if(countPost != null)
                return
            countPost = -3

            categoryDocument!!.get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error finding categories",context,it.exception, tagInput = tag)
                    return@addOnCompleteListener
                }
                val allCategories : List<String>? = it.result.toList(categoryField)
                //add index at the end of the category string
                val index = allCategories?.size ?: 0
                val newCategory = normalizeCategoryStrings(category,index)
                val newList : ArrayList<String> = if(allCategories != null) ArrayList(allCategories) else ArrayList()
                newList.add(newCategory)
                //send to firebase
                categoryDocument!!.update(categoryField,newList.toList()).addOnCompleteListener { task ->
                    if(!task.isSuccessful) showMessage("Error adding category",context,task.exception, tagInput = tag)
                    else {
                        countPost = countPost!!.plus(2)
                        postCategorySubcategoriesFinal(callback)
                    }
                }

                //set subcategories
                if(subcategories.isEmpty()){
                    countPost = countPost!!.plus(1)
                    postCategorySubcategoriesFinal(callback)
                    return@addOnCompleteListener
                }
                postSubcategoriesByCategory(category,subcategories.toList(),context,fun(){
                    countPost = countPost!!.plus(1)
                    postCategorySubcategoriesFinal(callback)
                })
            }
        }
        private fun postCategorySubcategoriesFinal(callback: () -> Unit){
            if(countPost!! >= 0){
                countPost = null
                callback()
            }
        }

        fun postSubcategoriesByCategory(category: String, subcategories: List<String>, context: Context?, callback: () -> Unit){
            getAllCategories(context,fun(allCategories:Array<String>){
                val index = allCategories.filter { c -> c.substringBefore("-").lowercase() == category.lowercase() }[0].substringAfter("-")

                getAllSubcategoriesByCategory(normalizeCategoryStrings(category,index.toInt()),context,fun(allSubcategories: List<String>){
                    val arrayList: ArrayList<String> = ArrayList(allSubcategories)
                    val compList: ArrayList<String> = ArrayList(arrayList.map { it.substringBefore("-").lowercase() })

                    subcategories.forEach { sub ->
                        val lowerSub = sub.lowercase()
                        showMessage(lowerSub, tagInput = "bbb")
                        if(!compList.contains(lowerSub)) {
                            showMessage("added", tagInput = "bbb")
                            arrayList.add(normalizeCategoryStrings(sub, arrayList.size))
                            compList.add(lowerSub)
                        }
                    }
                    if(arrayList == ArrayList(allSubcategories)){//nothing was added
                        callback()
                        return
                    }

                    postSubcategoryFinal(index,arrayList,context,callback)
                })

            })
        }
        private fun postSubcategoryFinal(index: String, subCategories: List<String>,context: Context?,callback: () -> Unit) {
            subcategoriesCollection.document(index).set(mapOf(subCategoryField to subCategories)).addOnCompleteListener {
                if(!it.isSuccessful) showMessage("Error posting subcategories",context,it.exception,tag)
                else callback()
            }
        }

        fun getAllCategories(context: Context?, callback: (Array<String>) -> Unit) {//TODO("Fabio guardati sto database :rimuovi indici usa posizioni")
            categoryDocument!!.get().addOnCompleteListener {
                if (!it.isSuccessful) {
                    showMessage("Error finding categories",context,it.exception, tagInput = tag)
                    return@addOnCompleteListener
                }
                val allCategories: List<String>? = it.result.toList(categoryField)
                if(allCategories == null) callback(arrayOf())
                else callback(allCategories.toTypedArray())
            }
        }

        fun getAllSubcategoriesByCategory(category: String, context: Context?, callback: (List<String>) -> Unit){
            subcategoriesCollection.document(category.substringAfter("-")).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error getting subcategories",context,it.exception, tagInput = tag)
                    return@addOnCompleteListener
                }
                val allSubcategories : List<String>? = it.result.toList(subCategoryField)
                if(allSubcategories == null) callback(listOf())
                else callback(allSubcategories)
            }
        }
        @Suppress("unused")
        fun putCategory(oldCategory: String, newCategory: String, context: Context?, callback: () -> Unit){
            getAllCategories(context,fun(allCategories: Array<String>){
                val arrayList : ArrayList<String> = ArrayList(allCategories.toList())
                if(!arrayList.contains(oldCategory)){
                    showMessage("Didn't find old category $oldCategory",context, tagInput = tag)
                }
                val index: Int = arrayList.indexOf(oldCategory)
                arrayList.remove(oldCategory)
                arrayList.add(index, normalizeCategoryStrings(newCategory.substringBefore("-"),index))

                categoryDocument!!.set(mapOf(categoryField to arrayList)).addOnCompleteListener {
                    if(!it.isSuccessful) showMessage("Error updating category",context,it.exception, tagInput = tag)
                    else callback()
                }
            })
        }
        @Suppress("unused")
        fun putSubcategory(category: String, oldSubcategory: String, newSubcategory: String, context: Context?, callback: () -> Unit){
            getAllSubcategoriesByCategory(category,context,fun(allSubcategories: List<String>){
                val arrayList: ArrayList<String> = ArrayList(allSubcategories)
                if (!arrayList.contains(oldSubcategory)) {
                    showMessage("Error finding old subcategory", tagInput = tag)
                    return
                }
                val index = arrayList.indexOf(oldSubcategory)
                arrayList.remove(oldSubcategory)
                arrayList.add(index, normalizeCategoryStrings(newSubcategory, index))

                postSubcategoryFinal(category,arrayList,context,callback)
            })
        }
//category
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//admin
        private const val statisticsCollectionName = "Statistics"
        private val statisticsCollection = Firebase.firestore.collection(statisticsCollectionName)
        fun getStatistics(context: Context?, callback: (Int, Float, List<User>, Int) -> Unit) {
            //return the number of products currently on sale,
            //the average time to sold (days), the top 3 users and how many items were sold
            statisticsCollection.document(productCollectionName).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error getting statistics",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                val documentData = it.result.data
                val avgTimeToSold: Float = documentData!!["avgTimeToSold"].toString().toFloat()
                val totItemsSold: Int = documentData["totItemsSold"].toString().toInt()
                // find top 3 users
                getTopUsersByRating(context,fun(topUsers: List<User>){
                    //find number of products on sale
                    getProductsByFilters(QueryArgs(isSold = false),context,fun(products: Array<Product>,_){
                        callback(products.size,avgTimeToSold,topUsers,totItemsSold)
                    })
                })
            }
        }
        private fun getTopUsersByRating(context: Context?, callback: (List<User>) -> Unit){
            userCollection
                .whereEqualTo("active",true)
                .whereEqualTo("admin",false)
                .orderBy("rating").limit(3)
                .get().addOnCompleteListener {
                    if(!it.isSuccessful){
                        showMessage("Error finding top users",context,it.exception,tag)
                        return@addOnCompleteListener
                    }
                    val documents = it.result.documents
                    callback(documents.map { doc -> doc.toUser() })
                }
        }
        private fun putProductStatistics(product: Product, context: Context?, callback: () -> Unit){
            //called when the user marks a product as sold
            getStatistics(context,fun(productsOnSale: Int, avgTimeToSold: Float, _, numItemsSold: Int){
                val newNumItemsSold = numItemsSold + 1
                val daysToSold: Int = Period.between(product.creationDate, LocalDate.now()).days
                val newAvgTime = (avgTimeToSold * numItemsSold + daysToSold) / newNumItemsSold
                //update statistics
                statisticsCollection.document(productCollectionName).update(
                    "productsOnSale",productsOnSale - 1,
                    "avgTimeToSold", newAvgTime,
                    "numItemsSold", newNumItemsSold
                ).addOnCompleteListener {
                    if(!it.isSuccessful) showMessage("Error updating statistics",context,it.exception,tag)
                    else callback()
                }
            })
        }

        fun freezeUser(userMail: String, context: Context?, callback: () -> Unit) {
            userCollection.document(userMail).update("active",false).addOnCompleteListener {
                if(!it.isSuccessful) showMessage("Error updating user",context,it.exception,tag)
                else callback()
            }
        }

        fun deleteUserByAdmin(userMail: String, context: Context?, callback: () -> Unit) {
            //delete chats
            //delete products
            deleteUserChats(User(email = userMail),context, fun(){//this calls delete products
                //delete favourite
                userCollection.document(userMail).collection(favouritesCollectionName).get().addOnCompleteListener {
                    if(!it.isSuccessful) showMessage("Error finding user",context,it.exception,tag)
                    else {
                        it.result.documents.forEach{el->
                            userCollection.document(userMail).collection(favouritesCollectionName).document(el.id).delete()
                        }
                        //freeze user
                        freezeUser(userMail,context,callback)
                    }
                }
            })
        }

        fun getUserProducts(userEmail: String, context: Context?, callback: (User,Array<Product>) -> Unit) {
            //check if the userEmail exists
            userCollection.document(userEmail).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error finding user",context,it.exception,tag)
                    return@addOnCompleteListener
                }
                if(it.result.data == null){
                    showMessage("No user was found",context, tagInput = tag)
                    return@addOnCompleteListener
                }
                val user: User = it.result.toUser()

                //get the user's products
                val queryArgs = QueryArgs(email = user.email)
                getProductsByFilters(queryArgs,context,fun(products:Array<Product>,_){
                    callback(user,products)
                })
            }
        }

        fun getProductsByProvince(province: String, context: Context?, callback: (List<Product>) -> Unit){
            getProductsByFilters(QueryArgs(),context,fun(products:Array<Product>,_){
                val result: ArrayList<Product> = ArrayList(products.toList())
                val lowerProvince = province.lowercase()
                result.removeIf { ! it.province.toString().lowercase().contains(lowerProvince) }
                callback(result.toList())
            })
        }

        private const val reviewCollectionName="Reviews"
        fun postReview(
            text: String,
            value: Int,
            mail: String,
            context: Context?,
            callback: () -> Unit,
        ) {
            showMessage("destinatario: $mail")
            userCollection.document(mail).collection(reviewCollectionName).document().set(
                mapOf("value" to value,"text" to text)
            ).addOnCompleteListener {
                if(it.isSuccessful){
                    userCollection.document(mail).get().addOnCompleteListener {task->
                        if(task.isSuccessful){
                            val user=task.result.toUser()
                            user.ratingNumber+=1
                            user.rating= if(user.ratingNumber > 1) (user.rating*(user.ratingNumber-1)+value)/user.ratingNumber else value.toFloat()
                            putStatisticsUser(user,context,callback)
                        }else{
                            showMessage("error reviewing user",context,task.exception)}
                    }
                      }else{
                    showMessage("Error postReview",context,it.exception,tag)
                }
            }
        }
        private fun putStatisticsUser(user: User, context: Context? ,callback: () -> Unit){
            userCollection.document(user.email).update(mapOf("ratingNumber" to user.ratingNumber,"rating" to user.rating)).addOnCompleteListener {
                if(!it.isSuccessful) showMessage("Error updating user selling number",context,it.exception,tag)
                else callback()
            }
        }
        fun getUserReviews(userMail: String,context: Context,callback: (List<Pair<Int, String>>) -> Unit){
            getUserReviews(context,userMail,fun(documents: List<DocumentSnapshot>){
                val reviews: List<Pair<Int, String>> = documents.map { t->
                    Pair(t.get("value").toString().toInt(), t.get("text").toString())
                }
                callback(reviews)
            })
        }
        private fun getUserReviews(context: Context, userMail: String, callback: (List<DocumentSnapshot>) -> Unit){
            userCollection.document(userMail).collection(reviewCollectionName).get().addOnCompleteListener {
                if(!it.isSuccessful){
                    showMessage("Error finding user reviews",context,it.exception, tag)
                    return@addOnCompleteListener
                }
                callback(it.result.documents)
            }
        }
//admin
    }
}
