package com.example.reuse

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import com.example.reuse.obj.Product
import com.example.reuse.obj.QueryArgs
import com.example.reuse.obj.User
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.math.cos

class Utility {
    companion object{
        fun showMessage(message: String, context: Context? = null, exception: Exception? = null, tagInput: String? = null){
            val tag = tagInput ?: "Utility"
            if(exception == null)
                Log.d(tag,message)
            else
                Log.e(tag,message,exception)
            if(context != null)
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

        fun makeAlertDialog(context: Context, title: String, positiveButtonText: String, negativeButtonText: String, message: String? = null, inflatedView: View? = null, positiveButtonCallback: DialogInterface.OnClickListener? = null, negativeButtonCallback: DialogInterface.OnClickListener? = null): AlertDialog {
            val builder = AlertDialog.Builder(context)
            builder.setCancelable(false)
            builder.setTitle(title)
            if(message != null)
                builder.setMessage(message)
            builder.setView(inflatedView)
            builder.setPositiveButton(positiveButtonText,positiveButtonCallback)
            builder.setNegativeButton(negativeButtonText,negativeButtonCallback)
            return builder.create()
        }
        private var notificationIndex = 0
        fun makeNotification(notificationManager: NotificationManager, context: Context, channelName: String, title: String, message: String, drawableIcon: Int){
            val channel = NotificationChannel("ReUse",channelName,NotificationManager.IMPORTANCE_DEFAULT)
            //channel.description = message
            notificationManager.createNotificationChannel(channel)
            val builder = NotificationCompat.Builder(context, "ReUse")
                .setSmallIcon(drawableIcon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            // Build the notification and send it
            notificationManager.notify(notificationIndex, builder.build())
            notificationIndex++
        }

        private fun distanceBetweenLocations(center: GeoPoint, point: GeoPoint): Double{
            val latCenter = Math.toRadians(center.latitude)
            val lonCenter = Math.toRadians(center.longitude)
            val latPoint = Math.toRadians(point.latitude)
            val lonPoint = Math.toRadians(point.longitude)
            val latDelta = latPoint - latCenter
            val lonDelta = lonPoint - lonCenter

            val a = Math.sin(latDelta / 2) * Math.sin(latDelta / 2) + Math.cos(latCenter) * Math.cos(latPoint) * Math.sin(lonDelta / 2) * Math.sin(lonDelta / 2)
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
            val earthRadius = 6371.0
            return earthRadius * c
        }
        fun filterProductsByQueryArgs(productsToFilter: List<Product>, queryArgs: QueryArgs): Array<Product>{
            val result: ArrayList<Product> = ArrayList(productsToFilter.toList())

            //the other case is already taken care of by the actual query
            if(queryArgs.range != null && queryArgs.latitude != null && queryArgs.longitude != null){
                val center = GeoPoint(queryArgs.latitude!!, queryArgs.longitude!!)
                result.removeIf {
                    val itCenter = GeoPoint(it.latitude, it.longitude)
                    val distance: Double = distanceBetweenLocations(center, itCenter)
                    distance > queryArgs.range!!
                }
            }

            if(queryArgs.name != null)
                result.removeIf { product: Product ->
                    !product.name.lowercase().contains(queryArgs.name!!.lowercase())
                }

            return result.toTypedArray()
        }
        fun filterProductsByQueryArgsForSnapshotBecauseWeDoNotCheatButHaveANecessity(productsToFilter: Array<Product>, queryArgs: QueryArgs): Array<Product>{
            val result: ArrayList<Product> = ArrayList(productsToFilter.toList())

            if(queryArgs.range != null && queryArgs.latitude != null && queryArgs.longitude != null){
                val center = GeoPoint(queryArgs.latitude!!,queryArgs.longitude!!)
                val latDelta = queryArgs.range!! / 111300f
                val lngDelta = queryArgs.range!! / (111300f * cos(center.latitude))
                result.removeIf{
                    val productWhere = GeoPoint(it.latitude,it.longitude)
                    productWhere.latitude < (center.latitude - latDelta)
                            || productWhere.longitude < (center.longitude - lngDelta)
                            || productWhere.latitude > (center.latitude + latDelta)
                            || productWhere.longitude > (center.longitude + lngDelta)
                }
            }
            if(queryArgs.priceSet && queryArgs.range == null) {
                //check min and max price
                if(queryArgs.minPrice != null)
                    //query = query.whereGreaterThanOrEqualTo("price",queryArgs.minPrice!!)
                    result.removeIf { it.price < queryArgs.minPrice!! }
                if(queryArgs.maxPrice != null)
                    //query = query.whereLessThanOrEqualTo("price",queryArgs.maxPrice!!)
                    result.removeIf { it.price > queryArgs.maxPrice!! }
            }
            if(queryArgs.shippable != null)
                //query = query.whereEqualTo("ship",queryArgs.shippable)
                result.removeIf { it.ship != queryArgs.shippable }

            return filterProductsByQueryArgs(result.toList(),queryArgs)
        }
    }
}

fun DocumentSnapshot.toList(field: String): List<String>? {
    return (this[field] as ArrayList<*>?)?.map { el -> el.toString() }
}

fun DocumentSnapshot.toUser(): User {
    val user = User()

    user.email = this["email"].toString()
    user.budget = this["budget"].toString().toFloat()
    user.isAdmin = this["admin"].toString().toBooleanStrict()
    user.rating = this["rating"].toString().toFloat()
    user.isActive = this["active"].toString().toBooleanStrict()
    user.ratingNumber = this["ratingNumber"].toString().toInt()

    return user
}

fun DocumentSnapshot.toProduct(): Product {
    val product = Product()

    product.id = this.id
    product.name = this["name"].toString()
    product.category = this["category"].toString()
    product.subCategory = this["subCategory"]?.toString()
    val geopoint = this["location"] as GeoPoint
    product.latitude = geopoint.latitude
    product.longitude = geopoint.longitude
    product.description = this["description"].toString()
    product.price = this["price"].toString().toFloat()
    product.status = this["status"].toString()
    product.ship = this["ship"].toString().toBooleanStrict()
    product.ownerMail = this["ownerMail"].toString()
    product.phone = this["phone"].toString()
    product.creationDate = LocalDate.parse(
        this["creationDate"].toString(),
        DateTimeFormatter.ofPattern("yyyy-MM-dd")
    )
    product.province = this["province"]?.toString()
    product.isSold = this["isSold"].toString().toBooleanStrict()
    //product.favourites = this["favourites"] as ...

    return product
}

fun DocumentSnapshot.toPair(user: User): Pair<Product,User> {
    val owner: String = this["owner"].toString()
    val product = Product(id = this["product"].toString())
    return if(owner != user.email) Pair(product,User(email = owner))
    else Pair(product,User(email = this["user"].toString()))
}

fun DocumentSnapshot.toQueryArgs(): QueryArgs {
    val queryArgs = QueryArgs()

    queryArgs.maxPrice = this["maxPrice"].toString().toFloatOrNull()
    queryArgs.minPrice = this["minPrice"].toString().toFloatOrNull()
    queryArgs.range = this["range"].toString().toIntOrNull()
    queryArgs.latitude = this["latitude"].toString().toDoubleOrNull()
    queryArgs.longitude = this["longitude"].toString().toDoubleOrNull()
    queryArgs.shippable = this["shippable"].toString().toBooleanStrictOrNull()
    queryArgs.priceSet = this["priceSet"].toString().toBooleanStrict()
    queryArgs.email = this["email"]?.toString()
    queryArgs.name = this["name"]?.toString()
    queryArgs.isSold = this["isSold"].toString().toBooleanStrictOrNull()
    queryArgs.productId = this["productId"].toString()

    return queryArgs
}
