package com.example.reuse.obj

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var email: String="",
    var rating: Float = 0F,
    var ratingNumber: Int = 0,
    var budget: Float = 0F,
    var isAdmin: Boolean = false,
    var isActive: Boolean = true
): Parcelable