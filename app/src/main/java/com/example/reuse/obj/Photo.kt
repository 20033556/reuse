package com.example.reuse.obj

import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Photo(val id: Long,val uri: Uri?):Comparable<Photo>, Parcelable {
    override fun compareTo(other: Photo): Int {
        return this.id.compareTo(other.id)
    }


}