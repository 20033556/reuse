package com.example.reuse.obj

import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import kotlinx.parcelize.Parcelize
import java.time.LocalDate


@Parcelize
data class Product(
    var id: String = "",
    var name: String = "",
    var category: String = "",
    var subCategory: String ?= null,
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    //var geopoint : GeoPoint = GeoPoint(0.0, 0.0),
    var description: String = "",
    var price: Float = 0F,
    var status: String = "",
    var ship: Boolean = false,
    var ownerMail: String = "",
    var phone: String = "",
    var creationDate: LocalDate = LocalDate.now(),
    var province: String? = null,//TODO togliere il null
    var isSold: Boolean = false
    //,var favourites: Collection<Firebase.firestore.user>
):Parcelable {
    fun toMap() : MutableMap<String, Any?>{
        val map = mutableMapOf<String, Any?>()
        map["name"] = name
        map["category"] = category
        map["subCategory"] = subCategory
        map["location"] = GeoPoint(latitude,longitude)
        map["description"] = description
        map["price"] = price
        map["status"] = status
        map["ship"] = ship
        map["ownerMail"] = ownerMail
        map["phone"] = phone
        map["creationDate"] = creationDate.toString()
        map["province"] = province
        map["isSold"] = isSold
        //map["favourites"] = favourites
        return map
        //be sure to update Utility as well (at the bottom)
    }

}