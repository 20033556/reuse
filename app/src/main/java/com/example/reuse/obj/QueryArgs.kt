package com.example.reuse.obj

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class QueryArgs (
    var name: String? = null,
    var minPrice: Float? = null,
    var maxPrice: Float? = null,
    var priceSet : Boolean = (minPrice != null || maxPrice != null),
    var latitude: Double? = null,
    var longitude: Double? = null,
    var range: Int? = null,
    var shippable: Boolean ?= null,
    var email:String?=null,
    var isSold: Boolean ?= null,
    var productId: String ?= null
): Parcelable {
    override fun toString(): String {
        return "$name\n$minPrice, $maxPrice, $priceSet\n$latitude, $longitude, $range\n$shippable\n$email\n$isSold\n$productId"
    }
}