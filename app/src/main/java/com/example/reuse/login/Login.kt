package com.example.reuse.login

import android.app.NotificationManager
import android.content.ContentValues.TAG
import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.obj.User
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Pattern

class Login : Fragment(R.layout.fragment_login) {
    private lateinit var auth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()
        val mailLogin: EditText = view.findViewById(R.id.mail)
        val passLogin: EditText = view.findViewById(R.id.password)
        val loginBtn: Button = view.findViewById(R.id.login)
        val registerBtn: Button= view.findViewById(R.id.toRegister)

        mailLogin.doAfterTextChanged {
            mailLogin.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.black))
            check(mailLogin, passLogin, loginBtn,registerBtn)
        }
        passLogin.doAfterTextChanged {
            passLogin.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.black))
            check(mailLogin, passLogin, loginBtn,registerBtn)
        }
        loginBtn.setOnClickListener {
            login(mailLogin, passLogin)
        }

        registerBtn.setOnClickListener{
            register(mailLogin, passLogin)
        }
    }

    private fun register(mailLogin: EditText, passLogin: EditText) {
        showMessage("Signing up...",context)
        auth.createUserWithEmailAndPassword(mailLogin.text.toString().lowercase(),passLogin.text.toString())
            .addOnCompleteListener { task ->
                Log.e(TAG, "task is ok")
                if (task.isSuccessful) {
                    Log.e(TAG, "task is successful")
                    val user = User(email = mailLogin.text.toString().lowercase())
                    Callout.postUser(user,context)
                    findNavController().navigate(LoginDirections.actionLoginToSearchPage())
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e(TAG, "createUserWithEmail:failure", task.exception)
                  showMessage("Authentication failed.", requireContext())
                }

            }


    }

    private fun login(mail: EditText, pass: EditText) {
        showMessage("Signing in...",context)
        auth.signInWithEmailAndPassword(mail.text.toString().lowercase(), pass.text.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    Callout.getUser(requireContext(),fun(user) {
                        if (user.isActive) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e(TAG, "signInWithEmail:success")
                            findNavController().navigate(LoginDirections.actionLoginToSearchPage())
                        } else {
                            findNavController().navigate(LoginDirections.actionLoginToBanPage())
                        }
                    },notificationManager)


                } else {
                    // If sign in fails, display a message to the user.
                    Log.e(
                        TAG,
                        "signInWithEmail:failure ${mail.text}, ${pass.text} ",
                        task.exception
                    )
                    showMessage("Failed to login", requireContext())
                }
            }
    }

    private fun check(mail: EditText, pass: EditText, loginBtn: Button, registerBtn: Button) {
        val pswPatter: Pattern = Pattern.compile(
            "^" +
                    "(?=.*[@#$%^&+=!*?])" +     // at least 1 special character
                    "(?=\\S+$)" +            // no white spaces
                    ".{8,}" +                // at least 8 characters
                    "$"
        )
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(mail.text.toString()).matches() &&
            pswPatter.matcher(pass.text.toString()).matches()
        ) {
            loginBtn.isEnabled = true
            registerBtn.isEnabled = true
        } else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mail.text.toString()).matches()) {
                mail.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.badRed))
            } else {
                if (!pswPatter.matcher(pass.text.toString()).matches() && pass.text.isNotEmpty()) {
                    pass.backgroundTintList =
                        ColorStateList.valueOf(ContextCompat.getColor(requireContext(),
                            R.color.badRed
                        ))
                }
            }
        }
    }

}