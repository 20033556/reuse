package com.example.reuse.search

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SwitchCompat
import androidx.navigation.fragment.findNavController
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.obj.Product
import com.example.reuse.obj.QueryArgs
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.GeoPoint


private const val tagg = "SearchPage"
class SearchPage : Fragment(R.layout.fragment_search_page) {

    private var userLastLocation: GeoPoint? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient// used to access user's location
    @SuppressLint("MissingPermission")
    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isPermissionGranted ->
        if(isPermissionGranted){
            fusedLocationClient.lastLocation.addOnCompleteListener{ task ->
                if(!task.isSuccessful) {
                    showMessage("Error getting user's last location",context,task.exception,tagg)
                    return@addOnCompleteListener
                }
                val location : Location? = task.result
                if(location == null){
                    showMessage("Couldn't find your last location",context, tagInput = tagg)
                    return@addOnCompleteListener
                }
                userLastLocation = GeoPoint(location.latitude,location.longitude)
            }
        }
    }

    private val notificationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {}
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //ask permission to send notifications
        notificationPermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS)
        val switch = view.findViewById<SwitchCompat>(R.id.shippable)
        var wasSwitchPressed = false
        switch.setOnClickListener{
            wasSwitchPressed = true
        }
        val rangeEditText = view.findViewById<EditText>(R.id.range)
        rangeEditText.setOnFocusChangeListener { _, _ ->
            askForLocationPermission()
        }

        view.findViewById<AppCompatButton>(R.id.confirmSearch).setOnClickListener {
            it.isEnabled = false
            val n = requireView().findViewById<EditText>(R.id.searchTitle).text.toString()
            val r = rangeEditText.text.toString()
            val minp = requireView().findViewById<EditText>(R.id.minPrice).text.toString()
            val maxp = requireView().findViewById<EditText>(R.id.maxPrice).text.toString()

            val name : String? = if (n != "") n else null
            val range : Int? = if (r != "") r.toInt() else null
            val minPrice : Float? = if (minp != "") minp.toFloat() else null
            val maxPrice : Float? = if (maxp != "") maxp.toFloat() else null
            val shippable : Boolean? = if (wasSwitchPressed) switch.isChecked else null

            if(range != null && userLastLocation == null){
                askForLocationPermission()//this is useless as it is async
            }
            val queryArgs = QueryArgs(name = name, range = range, latitude = userLastLocation?.latitude, longitude = userLastLocation?.longitude,minPrice = minPrice, maxPrice = maxPrice, shippable = shippable)
            if(queryArgs == QueryArgs()){
                showMessage("Insert at least one filter",context)
                it.isEnabled = true
                return@setOnClickListener
            }
            Callout.getProductsByFilters(queryArgs, context, ::showProductResult)
        }

    }

    private fun askForLocationPermission() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        locationPermissionRequest.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    private fun showProductResult(products: Array<Product>, usedQuery : QueryArgs){
        findNavController().navigate(SearchPageDirections.actionSearchPageToResultSearch(products,usedQuery))
    }
}
