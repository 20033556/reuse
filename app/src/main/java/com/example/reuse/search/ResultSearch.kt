package com.example.reuse.search

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.adapter.ProductAdapter
import com.example.reuse.firebase.Callout
import com.example.reuse.obj.Product
import com.example.reuse.obj.QueryArgs
import com.example.reuse.obj.User


class ResultSearch : Fragment(R.layout.fragment_result_search) {

    private val args: ResultSearchArgs by navArgs()
    private lateinit var queryArgs: QueryArgs

    private lateinit var user: User

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        queryArgs = args.queryArgs

        Callout.getUser(requireContext(),::setUser)

        showProducts(args.productList)
        view.findViewById<AppCompatButton>(R.id.refreshSearchResult).setOnClickListener {
            it.isEnabled = false
            //run the query again
            Callout.getProductsByFilters(queryArgs,context,fun (newProducts: Array<Product>, _){
                showMessage("Refeshed!",context)
                showProducts(newProducts)
                it.isEnabled = true
            })
        }
    }

    private fun showProducts(products: Array<Product>){
        if(products.isEmpty()){
            showMessage("No products matched your search.",context, tagInput = tag)
            findNavController().popBackStack()
            return
        }

        val recyclerView: RecyclerView = requireView().findViewById(R.id.resultSearchCardContainer)
        val adapter = ProductAdapter(context, products, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    private fun setUser(user: User){
        this.user = user
        //add on click listener for R.id.saveSearch
        val notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        requireView().findViewById<AppCompatButton>(R.id.saveSearch).setOnClickListener {
            it.isEnabled = false
            Callout.postSearch(user,queryArgs,requireContext(),notificationManager,fun(){
                showMessage("Search was saved!",context)
            })
        }
    }

}
