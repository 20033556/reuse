package com.example.reuse.productdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.firebase.Callout


class UserReview : Fragment(R.layout.fragment_user_review) {

    private val args: UserReviewArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView: RecyclerView = requireView().findViewById(R.id.reviewsCardContainer)
        recyclerView.layoutManager = LinearLayoutManager(context)

        Callout.getUserReviews(args.userMail,requireContext(),fun(reviews: List<Pair<Int,String>>){
            if(reviews.isNotEmpty())
                recyclerView.adapter = UserReviewAdapter(context, reviews)
            else {
                showMessage("No reviews found.",context)
                findNavController().popBackStack()
            }
        })
    }
}

private class UserReviewAdapter(context: Context?,private val reviews: List<Pair<Int,String>>): RecyclerView.Adapter<UserReviewAdapter.RecyclerHolder>(){
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
        val review = reviews[position]
        holder.textarea.text = review.second
        //fill up the correct number of stars
        for(i in 0 until review.first){
            when (i){
                0 -> {
                    holder.star0.setBackgroundResource(R.drawable.filled_star_icon)
                }
                1 -> {
                    holder.star1.setBackgroundResource(R.drawable.filled_star_icon)
                }
                2 -> {
                    holder.star2.setBackgroundResource(R.drawable.filled_star_icon)
                }
                3 -> {
                    holder.star3.setBackgroundResource(R.drawable.filled_star_icon)
                }
                4 -> {
                    holder.star4.setBackgroundResource(R.drawable.filled_star_icon)
                }
                else -> {}
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = mInflater.inflate(R.layout.review_item_view, parent, false)
        return RecyclerHolder(view)
    }

    override fun getItemCount() = reviews.size

    class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val star0: ImageView = itemView.findViewById(R.id.reviewStar0)
        val star1: ImageView = itemView.findViewById(R.id.reviewStar1)
        val star2: ImageView = itemView.findViewById(R.id.reviewStar2)
        val star3: ImageView = itemView.findViewById(R.id.reviewStar3)
        val star4: ImageView = itemView.findViewById(R.id.reviewStar4)
        val textarea: TextView = itemView.findViewById(R.id.reviewDetailText)
    }
}