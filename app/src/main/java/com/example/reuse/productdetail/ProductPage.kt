package com.example.reuse.productdetail


import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.DialogInterface
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.Utility
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.adapter.AdapterPhotos
import com.example.reuse.firebase.Callout
import com.example.reuse.firebase.Snapshot
import com.example.reuse.obj.Photo
import com.example.reuse.obj.Product
import com.example.reuse.obj.User
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.util.*


class ProductPage : Fragment(R.layout.fragment_product) {
    private lateinit var adapter: AdapterPhotos
    private lateinit var photos: ArrayList<Photo>

    private val  args: ProductPageArgs by navArgs()
    private lateinit var myLayoutInflater: LayoutInflater

    override fun onAttach(context: Context) {
        super.onAttach(context)
        myLayoutInflater = layoutInflater
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       val product=args.product
        val geocoder= Geocoder(requireContext(), Locale.ITALY)
        photos= ArrayList()
        adapter= AdapterPhotos(context,photos)

        //get the images
        val storageRef = Firebase.storage.reference
        val container=view.findViewById<RecyclerView>(R.id.ImgContainer)
        storageRef.child(product.id).listAll().addOnSuccessListener {doc->
            for(d in doc.items){
                storageRef.child(product.id+"/"+d.name).downloadUrl.addOnSuccessListener {
                    photos.add(
                        Photo(
                            d.name.toLong(),
                            it
                        )
                    )
                    adapter.notifyItemRangeInserted(photos.size-1,adapter.itemCount)
                }
            }
        }
        container.layoutManager=
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false)
        container.adapter=adapter

        val name=view.findViewById<TextView>(R.id.productName)
        name.text=product.name

        val category=view.findViewById<TextView>(R.id.productCategory)
        category.text=product.category.substringBefore("-")

        val subCategory=view.findViewById<TextView>(R.id.productSubCategory)
        subCategory.text=product.subCategory?.substringBefore("-")

        //get the location and the province
        val lis=Geocoder.GeocodeListener { pino ->
            this.requireActivity().runOnUiThread {
                val t = "${pino[0].locality},${product.province}"
                view.findViewById<TextView>(R.id.ProductPosition)?.text = t
            }
        }
        geocoder.getFromLocation(product.latitude,product.longitude,1,lis)

        val desc =view.findViewById<TextView>(R.id.productDescription)
        desc.text=product.description

        val ship=view.findViewById<CheckBox>(R.id.productShip)
        ship.isChecked=product.ship

        val state=view.findViewById<TextView>(R.id.productState)
        state.text=product.status

        val price=view.findViewById<TextView>(R.id.productPrice)
        price.text=product.price.toString()

        val mail=view.findViewById<TextView>(R.id.productOwnerMail)
        mail.text=product.ownerMail
        mail.setOnClickListener { findNavController().navigate(ProductPageDirections.actionProductToUserReview(product.ownerMail)) }

        val phone=view.findViewById<TextView>(R.id.productPhone)
        phone.text=product.phone

        val date=view.findViewById<TextView>(R.id.productCreationDate)
        date.text=product.creationDate.toString()


        val buyBtn=view.findViewById<Button>(R.id.buyBtn)
        val chatBtn=view.findViewById<ImageButton>(R.id.chatBtn)
        val modify=view.findViewById<Button>(R.id.creatorModifyBtn)
        val delete=view.findViewById<Button>(R.id.creatorDeleteBtn)

        //get the local user for controls
        Callout.getUser(requireContext(),fun(u){
            val favorite=requireView().findViewById<ImageButton>(R.id.addToFav)
            Callout.getFavourites(u,requireContext(),fun(products:List<String>){
                var favState=products.contains(product.id)
                if(favState){
                    favorite.background = ContextCompat.getDrawable(requireContext(),
                        R.drawable.filled_star_icon
                    )

                }else{
                    favorite.background = ContextCompat.getDrawable(requireContext(),
                        R.drawable.start_icon
                    )
                }

                favorite.setOnClickListener {
                    it.isEnabled = false
                    if(favState){
                        Callout.deleteFavourite(u,product,requireContext(),fun(){
                            favorite.background = ContextCompat.getDrawable(requireContext(),
                                R.drawable.start_icon
                            )
                            favState=false
                            it.isEnabled = true
                        })
                    }else{
                        val notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                        Callout.postFavourite(u,product,requireContext(),notificationManager,fun(){
                            favorite.background = ContextCompat.getDrawable(requireContext(),
                                R.drawable.filled_star_icon
                            )
                            favState=true
                            it.isEnabled = true
                        })

                    }
                }
            })
            //enable admin controll
            if(u.isAdmin){
                //admin can start a chat, modify, delete
                buyBtn.isEnabled=false
                favorite.visibility=View.GONE
                favorite.isEnabled = false
                modify.visibility=View.VISIBLE
                delete.visibility=View.VISIBLE
                modify.setOnClickListener {
                    it.isEnabled = false
                    findNavController().navigate(
                        ProductPageDirections.actionProductToModifyProduct(
                            u,
                            product,
                            photos.toTypedArray()
                        )
                    )
                }
                delete.setOnClickListener {
                    it.isEnabled = false
                    Callout.deleteProduct(product,requireContext(),fun(){findNavController().popBackStack()})
                }
                chatBtn.setOnClickListener {
                    it.isEnabled = false
                    Snapshot.postNewChat(product.id,product.ownerMail,u.email,context)
                    findNavController().navigate(
                        ProductPageDirections.actionProductToChatPage(
                            u,
                            product,
                            product.ownerMail
                        )
                    )
                }
                if(u.email == product.ownerMail){
                    chatBtn.visibility = View.GONE
                    chatBtn.isEnabled = false
                }
            }
           //enable creator controll
           else if( u.email== product.ownerMail){
               //owner can modify, delete, set to sold
                favorite.visibility=View.GONE
                favorite.isEnabled = false
                modify.visibility=View.VISIBLE
                delete.visibility=View.VISIBLE
                chatBtn.visibility=View.GONE
                chatBtn.isEnabled = false
                buyBtn.text=getString(R.string.setSold)

                modify.setOnClickListener {
                    it.isEnabled = false
                    findNavController().navigate(
                        ProductPageDirections.actionProductToModifyProduct(
                            u,
                            product,
                            photos.toTypedArray()
                        )
                    )
                }
                delete.setOnClickListener {
                    it.isEnabled = false
                    Callout.deleteProduct(product,requireContext(),fun(){findNavController().popBackStack()})
                }
                buyBtn.setOnClickListener {
                    it.isEnabled = false
                    review(u,product,true,buyBtn)
                }
           }
            else{
                // the user can buy if is not sold, add to favorite, start a chat
                if(product.isSold){
                    buyBtn.setText(R.string.itemSold)
                    buyBtn.isEnabled=false
                }else{
                    buyBtn.setOnClickListener {
                        it.isEnabled = false
                        review(u,product,false,buyBtn)
                    }
                }
                chatBtn.setOnClickListener {
                    it.isEnabled = false
                    Snapshot.postNewChat(product.id,product.ownerMail,u.email,context)
                    findNavController().navigate(
                        ProductPageDirections.actionProductToChatPage(
                            u,
                            product,
                            product.ownerMail
                        )
                    )
                }
            }
        })
    }
   //review set up
    @SuppressLint("InflateParams")
    private fun review(u: User, product: Product, isOwner: Boolean, buyBtn: Button) {
        buyBtn.isEnabled = false
        showMessage("inflater: "+myLayoutInflater)
        //https://web.archive.org/web/20190528213406/https://possiblemobile.com/2013/05/layout-inflation-as-intended/
        val reviewPage=myLayoutInflater.inflate(R.layout.review,null,false)
        val text=reviewPage.findViewById<EditText>(R.id.reviewText)
        val value=reviewPage.findViewById<EditText>(R.id.reviewValue)
        val email=reviewPage.findViewById<EditText>(R.id.reviewMail)

        if (u.email==product.ownerMail){
            email.visibility=View.VISIBLE
        } else email.setText(product.ownerMail)
        val myContext = requireContext()
        val positiveCall = DialogInterface.OnClickListener{ _, _ ->
            if(text.text.toString().isBlank() || value.text.toString().isBlank() || email.text.toString().isBlank()){
                showMessage("Fill in every field",myContext)
                buyBtn.isEnabled = true
                return@OnClickListener
            }
            if(value.text.toString().toInt()<1 || value.text.toString().toInt()>5){
                buyBtn.isEnabled = true
                return@OnClickListener
            }

            if(isOwner){
                Callout.putProductToSold(product,context,fun(){
                    showMessage("Sold ${product.name}",context)
                    postReview(text.text.toString(),value.text.toString().toInt(),email.text.toString(),context)
                })
            } else {
                Callout.buyProduct(product,u,context,fun(){
                    showMessage("Bought ${product.name}",context)
                    postReview(text.text.toString(),value.text.toString().toInt(),email.text.toString(),context)
                })
            }
        }
        val negativeCall = DialogInterface.OnClickListener { _, _ -> buyBtn.isEnabled = true }
        Utility.makeAlertDialog(
            requireContext(),
            getString(R.string.write_review),
            getString(R.string.ok),
            getString(R.string.cancel),
            null,
            reviewPage,
            positiveCall,
            negativeCall
        ).show()
    }

    private fun postReview(text: String, value: Int, email: String, context: Context?){
        Callout.postReview(text,value,email,context,fun(){
            showMessage(
                "Thanks!!",
                context
            )
        })
    }

}
