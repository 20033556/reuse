package com.example.reuse.personalpage

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.Utility.Companion.makeAlertDialog
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.adapter.ProductAdapter
import com.example.reuse.obj.Product
import com.example.reuse.obj.User
import java.util.regex.Pattern

class PersonalPage : Fragment(R.layout.fragment_personal_page) {

  private lateinit var user: User

  private val favourites: ArrayList<Product> = ArrayList()
  private lateinit var addFundsBtn: AppCompatButton
  private lateinit var deleteBtn: AppCompatButton
  private lateinit var signoutBtn: AppCompatButton
  private lateinit var reviewsBtn: AppCompatButton

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    Callout.getUser(requireContext(),fun (user: User){
      if(user.isAdmin){
        findNavController().navigate(PersonalPageDirections.actionPersonalPageToAdminPanel())
        return
      }

      this.user = user
      view.findViewById<TextView>(R.id.userMail).text = user.email
      view.findViewById<TextView>(R.id.userBudgetValue).text = user.budget.toString()

      Callout.getFavourites(user,requireContext(),fun(favouriteIds: List<String>){
        var favIndex = 0
        val totalFavourites = favouriteIds.size
        favouriteIds.forEach {
          Callout.getProductById(it,context,fun(newFavourite:Product){
            if(!favourites.contains(newFavourite))
              favourites.add(newFavourite)
            favIndex++
            if(favIndex == totalFavourites)
              showFavourites(favourites.toTypedArray())
          })
        }
      })
    })

    addFundsBtn = view.findViewById(R.id.addFundsBtn)
    deleteBtn = view.findViewById(R.id.deleteAccountBtn)
    signoutBtn = view.findViewById(R.id.signOut)
    reviewsBtn = view.findViewById(R.id.viewReviews)

    addFundsBtn.setOnClickListener {
      setButtonsStatus(false)
      //launch add funds fragment
      findNavController().navigate(PersonalPageDirections.actionPersonalPageToAddFunds())
    }
    deleteBtn.setOnClickListener {
      setButtonsStatus(false)
      askPassword()
    }

    signoutBtn.setOnClickListener {
      setButtonsStatus(false)
      Callout.signOut(requireContext(),requireActivity())
      findNavController().navigate(PersonalPageDirections.actionPersonalPageToLogin())
    }

    reviewsBtn.setOnClickListener {
      setButtonsStatus(false)
      findNavController().navigate(PersonalPageDirections.actionPersonalPageToUserReview(user.email))
    }
  }

  private fun setButtonsStatus(enable: Boolean){
    signoutBtn.isEnabled = enable
    deleteBtn.isEnabled = enable
    addFundsBtn.isEnabled = enable
    reviewsBtn.isEnabled = enable
  }

  private fun showFavourites(favourites: Array<Product>){
    val recyclerView: RecyclerView = requireView().findViewById(R.id.favouritesCardContainer)
    val adapter = ProductAdapter(context, favourites, this)
    recyclerView.layoutManager = LinearLayoutManager(context)
    recyclerView.adapter = adapter
  }

  @SuppressLint("InflateParams")
  private fun askPassword(){
    //show dialog to confirm; then delete account from firebase

    //create a password input field
    //https://web.archive.org/web/20190528213406/https://possiblemobile.com/2013/05/layout-inflation-as-intended/
    val passwordInputView: View = layoutInflater.inflate(R.layout.password_input, null  ,false)

    val passwordEditText = passwordInputView.findViewById<EditText>(R.id.passwordEditText)

    passwordEditText.addTextChangedListener(object: TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        passwordEditText.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(),
              if(isValidPassword(passwordEditText))
                R.color.purple
              else R.color.badRed
            ))
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })

    val positiveCall = DialogInterface.OnClickListener{ _, _ ->
      // Perform delete account action
      if(!isValidPassword(passwordEditText)) {
        showMessage(getString(R.string.passwordError),context, tagInput = tag)
        setButtonsStatus(true)
        return@OnClickListener
      }

      val password = passwordEditText.text.toString()
      Callout.deleteUserRelatedData(user, password, requireContext(),requireActivity(), fun (){
        showMessage("User was deleted.",context)
      }, ::askPassword)
    }

    makeAlertDialog(
      requireContext(),
      getString(R.string.deleteAccount),
      getString(R.string.yes),
      getString(R.string.no),
      getString(R.string.deleteAccountDialog),
      passwordInputView,
      positiveCall
    ).show()
  }

  private fun isValidPassword(password: EditText) : Boolean {
    val pswPatter: Pattern = Pattern.compile(
      "^" +
              "(?=.*[@#$%^&+=!*?])" +     // at least 1 special character
              "(?=\\S+$)" +            // no white spaces
              ".{8,}" +                // at least 8 characters
              "$"
    )
    return pswPatter.matcher(password.text.toString()).matches()
  }
}
