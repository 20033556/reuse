package com.example.reuse.personalpage.admin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.Utility
import com.example.reuse.adapter.ProductAdapter
import com.example.reuse.firebase.Callout
import com.example.reuse.obj.Product
import com.example.reuse.obj.User

class AdminUserDetail : Fragment(R.layout.fragment_admin_user_detail) {

    private val args: AdminUserDetailArgs by navArgs()
    private lateinit var searchedUserMail: User

    private lateinit var userMailEditText: EditText
    private lateinit var userProductsNumber: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchedUserMail = args.searchedUser
        val products: Array<Product> = args.relatedProducts
        showProducts(products)

        userMailEditText = view.findViewById(R.id.searchUserEditText)
        userMailEditText.text.clear()
        userMailEditText.text.insert(0,searchedUserMail.email)
        val freezeBtn = view.findViewById<AppCompatButton>(R.id.freeze)
        val deleteBtn = view.findViewById<AppCompatButton>(R.id.delete)
        val searchBtn = view.findViewById<AppCompatButton>(R.id.searchBtn)
        userMailEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                freezeBtn.isEnabled = false
                deleteBtn.isEnabled = false
                searchBtn.isEnabled = true
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        userProductsNumber = view.findViewById(R.id.userProducts)
        userProductsNumber.text = products.size.toString()

        //set buttons
        freezeBtn.setOnClickListener {
            it.isEnabled = false
            val user : String = userMailEditText.text.toString()
            if(user.isBlank()) {
                it.isEnabled = true
                return@setOnClickListener
            }
            Callout.freezeUser(user, context, fun() {
                Utility.showMessage("User was frozen", context)
            })
        }
        deleteBtn.setOnClickListener {
            it.isEnabled = false
            val user : String = userMailEditText.text.toString()
            if(user.isBlank()){
                it.isEnabled = true
                return@setOnClickListener
            }
            Callout.deleteUserByAdmin(user,context,fun(){
                Utility.showMessage("User was deleted", context)
            })
        }
        searchBtn.setOnClickListener {
            it.isEnabled = false
            val user: String = userMailEditText.text.toString()
            if(user.isBlank()){
                it.isEnabled = true
                return@setOnClickListener
            }
            // navigate to user products
            Callout.getUserProducts(user,context,fun(_,products: Array<Product>){
                freezeBtn.isEnabled = true
                deleteBtn.isEnabled = true
                showProducts(products)
            })
        }
    }

    private fun showProducts(products: Array<Product>){
        if(products.isEmpty()){
            Utility.showMessage("No products matched your search.", context, tagInput = tag)
            return
        }

        val recyclerView: RecyclerView = requireView().findViewById(R.id.adminCardContainer)
        val adapter = ProductAdapter(context, products, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }
}