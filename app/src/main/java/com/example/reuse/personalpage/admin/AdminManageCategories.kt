package com.example.reuse.personalpage.admin

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.AppCompatButton
import com.example.reuse.R
import com.example.reuse.Utility.Companion.makeAlertDialog
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.firebase.Callout


class AdminManageCategories : Fragment(R.layout.fragment_admin_manage_categories) {

    private lateinit var categoryEditText: EditText
    private lateinit var subcategoriesEditText: EditText

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryEditText = view.findViewById(R.id.adminNewCategory)
        subcategoriesEditText = view.findViewById(R.id.adminNewSubcategories)

        view.findViewById<AppCompatButton>(R.id.adminConfirmCategories).setOnClickListener {
            it.isEnabled = false
            val category = categoryEditText.text.toString()
            if(category.isBlank()){
                showMessage("Insert a category",context)
                it.isEnabled = true
                return@setOnClickListener
            }
            val subcategories = ArrayList(subcategoriesEditText.text.toString().split("\n"))
            subcategories.removeIf { s -> s.isBlank() }
            showConfirmation(category,subcategories.distinct(),it)
        }
    }

    private fun showConfirmation(category: String, subcategories: List<String>, confirmBtn: View){
        val message = StringBuilder()
            .append(getString(R.string.category)).append("\n")
            .append(category).append("\n")
            .append(getString(R.string.subcategory)).append("\n")
        if(subcategories.isNotEmpty()){
            var index = 0
            val max = subcategories.size - 1
            subcategories.forEach { if(index < max){
                message.append(it).append(", ")
                index++
            } else message.append(it) }
        }
        else message.append(getString(R.string.notAvailable))

        val positiveListener = DialogInterface.OnClickListener{ _, _ ->
            Callout.getAllCategories(context,fun(categoriesAvailable: Array<String>){
                val cats = categoriesAvailable.map { it.substringBefore("-").lowercase() }

                if(cats.contains(category.lowercase())){
                    //post the subcategories
                    showMessage("Posting subcategories...",context)
                    Callout.postSubcategoriesByCategory(category,subcategories,context,fun(){
                        showMessage("Subcategories posted",context)
                        confirmBtn.isEnabled = true
                        categoryEditText.text.clear()
                        subcategoriesEditText.text.clear()
                    })
                } else {
                    //the admin is trying to create a new category with some subcategories
                    showMessage("Creating cat-sub...",context)
                    Callout.postCategorySubcategoriesBeginning(category,subcategories.toTypedArray(),context,fun(){
                        showMessage("New cat-subcat posted",context)
                        confirmBtn.isEnabled = true
                        categoryEditText.text.clear()
                        subcategoriesEditText.text.clear()
                    })
                }
            })
        }

        val negativeListener = DialogInterface.OnClickListener{ _, _ ->
            confirmBtn.isEnabled = true
        }

        makeAlertDialog(
            requireContext(),
            getString(R.string.isThisCorrect),
            getString(R.string.yes),
            getString(R.string.no),
            message.toString(),
            null,
            positiveListener,
            negativeListener
        ).show()
    }
}