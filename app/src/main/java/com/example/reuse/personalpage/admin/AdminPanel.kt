package com.example.reuse.personalpage.admin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.reuse.R
import com.example.reuse.firebase.Callout
import com.example.reuse.obj.Product
import com.example.reuse.obj.User


class AdminPanel : Fragment(R.layout.fragment_admin_panel) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sellingNumberView = view.findViewById<TextView>(R.id.sellingNumber)
        val averageSellTimeView = view.findViewById<TextView>(R.id.averageTime)
        val bestSellersView = view.findViewById<TextView>(R.id.bestSellers)

        Callout.getStatistics(context,fun (sellingNumber: Int, averageSellTime : Float, bestSellingUsers: List<User>, _){
                //sellingNumber
                sellingNumberView.text = sellingNumber.toString()
                //averageSellTime
                val timeString = averageSellTime.toString()
                averageSellTimeView.text = timeString.substring(0,timeString.indexOf(".")+3)
                //bestSellingUsers
                val bsUsers : StringBuilder = StringBuilder()
                bestSellingUsers.forEach { user : User ->
                    val mail = if(user.email.length >= 15)user.email.subSequence(0,15) else user.email
                    if(bsUsers != StringBuilder())
                        bsUsers.append("\n").append(mail)
                    else
                        bsUsers.append(mail)
                }

                bestSellersView.text = bsUsers.toString()
            })

        val searchUser: EditText = view.findViewById(R.id.searchUser)
        val searchBtn = view.findViewById<AppCompatButton>(R.id.searchBtn)
        
        searchUser.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchBtn.isEnabled = true
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        searchBtn.setOnClickListener {
            searchBtn.isEnabled = false
            val userMail : String = searchUser.text.toString()
            if(userMail.isBlank()){
                searchBtn.isEnabled = true
                return@setOnClickListener
            }
            Callout.getUserProducts(userMail,context,fun(user: User,products: Array<Product>){
                findNavController().navigate(AdminPanelDirections.actionAdminPanelToAdminUserDetail(user,products))
            })
        }

        val provinceEditText: EditText = view.findViewById(R.id.searchProvinceText)
        val provinceNumber: TextView = view.findViewById(R.id.provinceNumber)

        view.findViewById<AppCompatButton>(R.id.searchProvinceBtn).setOnClickListener {
            it.isEnabled = false
            val province = provinceEditText.text.toString()
            if(province.isBlank()){
                it.isEnabled = true
                return@setOnClickListener
            }
            Callout.getProductsByProvince(province,context,fun(products:List<Product>){
                provinceNumber.text = products.size.toString()
                it.isEnabled = true
            })
        }

        view.findViewById<AppCompatButton>(R.id.signOut).setOnClickListener {
            it.isEnabled = false
            Callout.signOut(requireContext(),requireActivity())
        }

        view.findViewById<AppCompatButton>(R.id.createCategory).setOnClickListener {
            it.isEnabled = false
            findNavController().navigate(AdminPanelDirections.actionAdminPanelToAdminManageCategories())
        }
    }

}