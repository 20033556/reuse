package com.example.reuse.personalpage

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage

class AddFunds : Fragment(R.layout.fragment_add_funds) {

  private lateinit var creditCardInput: AppCompatEditText //creditCardInput
  private lateinit var cvcInput: AppCompatEditText //cvcInput
  private lateinit var dateInput: AppCompatEditText //dateInput
  private lateinit var holderInput: AppCompatEditText //holderInput
  private lateinit var amountInput: AppCompatEditText //amountInput
  private lateinit var addBtn: AppCompatButton //confirmBtn

  //this var is an array which contains the edit text which have passed the checks
  private val validInputs = ArrayList<AppCompatEditText>()


  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    Callout.getUser(requireContext(), fun(user){
      addBtn = view.findViewById(R.id.confirmBtn)
      addBtn.isEnabled = false
      addBtn.setOnClickListener {
        it.isEnabled = false
        //check if all the inputs are valid
        if (validInputs.size == 5) {
          //add funds to the user's budget
          Callout.putUserBudget(user,amountInput.text.toString().toFloat(), context, fun(){findNavController().popBackStack()})
        } else {
          //show error message
          showMessage("Fill in all fields",context)
        }
      }
    })

    creditCardInput = view.findViewById(R.id.creditCardInput)
    cvcInput = view.findViewById(R.id.cvcInput)
    dateInput = view.findViewById(R.id.dateInput)
    holderInput = view.findViewById(R.id.holderInput)
    amountInput = view.findViewById(R.id.amountInput)
    addListenerChecks()
  }

  private fun addListenerChecks() {
    creditCardInput.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        if (s.toString().isCreditCard()) {
          creditCardInput.error = null
          if(!validInputs.contains(creditCardInput))
            validInputs.add(creditCardInput)
          addBtn.isEnabled = validInputs.size == 5
        } else {
          creditCardInput.error = "Invalid credit card"
          validInputs.remove(creditCardInput)
        }
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })

    cvcInput.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        if (s.toString().isCVC()) {
          cvcInput.error = null
          if(!validInputs.contains(cvcInput))
            validInputs.add(cvcInput)
          addBtn.isEnabled = validInputs.size == 5
        } else {
          cvcInput.error = "Invalid CVC"
          validInputs.remove(cvcInput)
        }
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //do nothing
      }

      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //do nothing
      }
    })

    dateInput.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        if (s.toString().isDate()) {
          dateInput.error = null
          if(!validInputs.contains(dateInput))
            validInputs.add(dateInput)
          addBtn.isEnabled = validInputs.size == 5
        } else {
          dateInput.error = "Invalid date"
          validInputs.remove(dateInput)
        }
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //do nothing
      }

      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //do nothing
      }
    })

    holderInput.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        if (s.toString().isName()) {
          holderInput.error = null
          if(!validInputs.contains(holderInput))
            validInputs.add(holderInput)
          addBtn.isEnabled = validInputs.size == 5
        } else {
          holderInput.error = "Invalid name"
          validInputs.remove(holderInput)
        }
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int){
        //do nothing
      }
      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //do nothing
      }
    })

    amountInput.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {
        if (s.toString().isAmount()) {
          amountInput.error = null
          if(!validInputs.contains(amountInput))
            validInputs.add(amountInput)
          addBtn.isEnabled = validInputs.size == 5
        } else {
          amountInput.error = "Invalid amount"
          validInputs.remove(amountInput)
        }
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //do nothing
      }

      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //do nothing
      }
    })
  }

  fun String.isCreditCard(): Boolean {
    //return this.matches(Regex("^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$"))
    return this.matches(Regex("^\\d{16}$"))
  }

  fun String.isCVC(): Boolean {
    return this.matches(Regex("^\\d{3,4}$"))
  }

  fun String.isDate(): Boolean {
    return this.matches(Regex("^(0[1-9]|1[0-2])/?(\\d{4}|\\d{2})$"))
  }

  fun String.isName(): Boolean {
    return this.matches(Regex("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*\$"))
  }

  fun String.isAmount(): Boolean {
    return this.matches(Regex("^\\d+(\\.\\d{1,2})?\$"))
  }

}
