package com.example.reuse.userProduct

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageButton
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.firebase.Callout
import com.example.reuse.R
import com.example.reuse.adapter.ProductAdapter


class MyProducts : Fragment(R.layout.fragment_my_products) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<ImageButton>(R.id.addBtn).setOnClickListener{
            it.isEnabled = false
            findNavController().navigate(MyProductsDirections.actionMyProductsToAddProduct())
        }
        val container=view.findViewById<RecyclerView>(R.id.myProductsCardContainer)
        Callout.getMyProducts(context,fun(products){
            val adapter = ProductAdapter(context,products,this)
            container.layoutManager = LinearLayoutManager(context)
            container.adapter = adapter
        })
    }
}