package com.example.reuse.userProduct

import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.R
import com.example.reuse.Utility.Companion.showMessage
import com.example.reuse.adapter.AdapterPhotos
import com.example.reuse.firebase.Callout
import com.example.reuse.obj.Photo
import com.example.reuse.obj.Product
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.util.*


class ModifyProduct : Fragment(R.layout.fragment_modify_product) {
    private lateinit var photos: ArrayList<Photo>
    private lateinit var adapter: AdapterPhotos
    private lateinit var subCategories:Array<String>
    private lateinit var categoryVal:String
    private var subCategoryVal:String=""
    private var stateVal="nuovo"
    private val  args: ModifyProductArgs by navArgs()
    private lateinit var addImg:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val container=  view.findViewById<RecyclerView>(R.id.ImgAddContainer)
        addImg=view.findViewById(R.id.AddImgBtn)
        val modify=view.findViewById<Button>(R.id.creatorModifyBtn)

        val nameV=view.findViewById<EditText>(R.id.productName)
        nameV.setText(args.product.name)
        val category=view.findViewById<Spinner>(R.id.productCategory)
        val subCategory=view.findViewById<Spinner>(R.id.productSubCategory)
        val productCityV=view.findViewById<EditText>(R.id.productLocation)
        val descV=view.findViewById<EditText>(R.id.productDescription)
        descV.setText(args.product.description)
        val priceV=view.findViewById<EditText>(R.id.productPrice)
        priceV.setText(args.product.price.toString())
        val state=view.findViewById<Spinner>(R.id.productState)
        val shipV=view.findViewById<CheckBox>(R.id.productShip)
        shipV.isChecked=args.product.ship
        val phoneV=view.findViewById<EditText>(R.id.productPhone)
        phoneV.setText(args.product.phone)

        var latitude=args.product.latitude
        var longitude=args.product.longitude



        //categories and subCategories management get values and set spinner items and listener
        Callout.getAllCategories (requireContext(), fun(categories:Array<String>){
            val adapter= ArrayAdapter(requireContext(),android.R.layout.simple_spinner_item, categories.map { el -> el.substringBefore("-") })
            category.adapter = adapter
            category.setSelection(categories.indexOf(args.product.category))
            category.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {
                    categoryVal=categories[position]
                    subCategory.visibility= View.VISIBLE
                    getSub(categoryVal,subCategory)
                }
                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
            subCategory.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {
                    subCategoryVal=subCategories[position] }
                override fun onNothingSelected(parent: AdapterView<*>) {}
            }

            state.onItemSelectedListener=object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {
                    val items= resources.getStringArray(R.array.product_states)
                    stateVal= items[position]}
                override fun onNothingSelected(parent: AdapterView<*>) {}
            }

           state.setSelection(resources.getStringArray(R.array.product_states).indexOf(args.product.status))
        }
        )
        val geocoder= Geocoder(requireContext(), Locale.ITALY)
        val col=Geocoder.GeocodeListener { pino ->
            this.requireActivity().runOnUiThread {
                val t = pino[0].locality
                view.findViewById<EditText>(R.id.productLocation)?.setText(t)
            }
        }
        geocoder.getFromLocation(args.product.latitude,args.product.longitude,1,col)
        // Create a storage to store images
        val storageRef = Firebase.storage.reference


        //set up image retrieval
        photos= ArrayList(args.photo.toList())
        val size=photos.size
        adapter= AdapterPhotos(context,photos)

        container.layoutManager=
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false)
        container.adapter=adapter

        addImg.setOnClickListener {
            addImg.isEnabled = false
            openCustomGallery()
        }

        //save product on db after check required fields
        modify.setOnClickListener {
            modify.isEnabled = false
            val ship=shipV.isChecked
            val desc=descV.text.toString()
            val name=nameV.text.toString()
            val price=priceV.text.toString()
            val phone=phoneV.text.toString()
            val productCity=productCityV.text.toString()
            if(productCity.isBlank()){
                modify.isEnabled = true
                showMessage("Insert a location.",context)
                return@setOnClickListener
            }
            val lisProv= Geocoder.GeocodeListener {
                val province=it[0].subAdminArea

                if(check(name,categoryVal,desc,price,stateVal,phone,latitude,longitude)){
                    val p= Product(args.product.id,
                        name,
                        categoryVal,
                        subCategoryVal,
                        latitude,
                        longitude,
                        desc,
                        price.toFloat(),
                        stateVal,
                        ship,
                        args.product.ownerMail,
                        args.product.phone,
                        args.product.creationDate,
                        province =  province.toString().substringAfter("Provincia di ").substringAfter("Città Metropolitana di "))
                    Callout.putProduct(args.product,p,requireContext(),fun(p){
                        for (i in size until photos.size){
                            val imagesRef = storageRef.child(p.id+"/" +photos[i].uri?.lastPathSegment)
                            imagesRef.putFile(photos[i].uri!!)}
                    })

                    this.requireActivity().runOnUiThread {
                        findNavController().popBackStack()
                        findNavController().popBackStack()
                    }
                } else {
                    this.requireActivity().runOnUiThread {
                        modify.isEnabled = true
                        showMessage("Fill in all fields.",context)
                    }
                }
            }

            //set location
            val lis= Geocoder.GeocodeListener {
                if(it.isNotEmpty()){
                    longitude=it[0].longitude
                    latitude=it[0].latitude
                    geocoder.getFromLocation(latitude,longitude,1,lisProv)
                } else {
                    this.requireActivity().runOnUiThread {
                        modify.isEnabled = true
                        showMessage("Error with location",context)
                    }
                }
            }
            geocoder.getFromLocationName(productCity,1, lis)
        }

    }

    private fun check(
        name: String,
        categoryVal: String,
        desc: String,
        price: String,
        stateVal: String,
        phone: String,
        latitude: Double,
        longitude: Double
    ): Boolean {
        if (name.isEmpty() || categoryVal.isEmpty() || desc.isEmpty() || price.isEmpty() ||
            stateVal.isEmpty() || phone.isEmpty() || latitude==0.0 || longitude==0.0) return false
        return true
    }

    private fun getSub(categoryVal: String, subCategory: Spinner) {
        Callout.getAllSubcategoriesByCategory(categoryVal,requireContext(),fun(subs:List<String>){
            val adapter= ArrayAdapter(requireContext(),android.R.layout.simple_spinner_item, subs.map { el -> el.substringBefore("-") })
            subCategory.adapter = adapter
            subCategories=subs.toTypedArray()
            subCategory.setSelection(subCategories.indexOf(args.product.subCategory))
            if(subs.isEmpty()) subCategory.visibility= View.GONE
        })
    }


    // Registers a photo picker activity launcher in multi-select mode.
    // In this example, the app allows the user to select up to 5 media files.
    private val pickMultipleMedia =
        registerForActivityResult(ActivityResultContracts.PickMultipleVisualMedia(3)) { uris ->
            if (uris.isNotEmpty()) {
                Thread {
                    for (i in 0 until uris!!.size) {
                        photos.add(
                            Photo(
                                adapter.n.toLong() + 1,
                                uris[i]
                            )
                        )
                        adapter.n++
                        requireActivity().runOnUiThread {
                            adapter.notifyItemInserted(adapter.itemCount)
                            addImg.isEnabled = true
                        }
                    }
                }.start()
            }else {
                requireActivity().runOnUiThread{
                    addImg.isEnabled = true
                }
            }
        }

    private fun openCustomGallery() {
        pickMultipleMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageAndVideo))
    }

}