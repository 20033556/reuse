package com.example.reuse.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.reuse.NavGraphDirections
import com.example.reuse.R
import com.example.reuse.obj.Product

class ProductAdapter(
    val context: Context?,
    private val products: Array<Product>,
    private val frag: Fragment
) : RecyclerView.Adapter<ProductAdapter.RecyclerHolder>() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var relPosition = 0

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
        val product: Product = products[position]
        holder.name.text = product.name
        holder.category.text = product.category.substringBefore("-")
        holder.price.text = product.price.toString()
        holder.sold.visibility  = if(product.isSold) View.VISIBLE else View.GONE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = mInflater.inflate(R.layout.product_item_view, parent, false)
        val holder = RecyclerHolder(view)
        val position = relPosition
        view.setOnClickListener {
            val product = products[position]
            findNavController(frag.requireActivity(),R.id.nav_host_fragment).navigate(NavGraphDirections.actionGlobalProduct(product))
        }
        relPosition++
        return holder
    }

    override fun getItemCount(): Int {
        return products.size
    }

    class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.itemName)
        val category: TextView = itemView.findViewById(R.id.itemCategory)
        val price: TextView = itemView.findViewById(R.id.itemPrice)
        val sold: TextView = itemView.findViewById(R.id.itemSold)
    }

}