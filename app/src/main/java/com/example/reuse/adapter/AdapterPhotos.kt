package com.example.reuse.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.reuse.R
import com.example.reuse.obj.Photo

class AdapterPhotos(val context: Context?, val photos: ArrayList<Photo>) : RecyclerView.Adapter<AdapterPhotos.RecyclerHolder>() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    var n: Int = 0

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {
        if(photos[position].id > n)
            n = photos[position].id.toInt()
        Glide.with(context!!).load(photos[position].uri).into(holder.photo)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = mInflater.inflate(R.layout.photo_item_view, parent, false)
        return RecyclerHolder(view)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo: ImageView = itemView.findViewById(R.id.imageview)
    }

}