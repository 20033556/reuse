# ReUse
### Progetto Android
## Sviluppatori
- Milanesio Fabio 20033556
- Chissotti Alfredo 20033498
## Requisiti
Il progetto riguarda la realizzazione di una app per la vendita di oggetti usati e deve prevedere due tipi di accessi: amministratore e utente.
Ogni oggetto è definito dalle seguenti informazioni:

- nome dell'oggetto
- categoria o sotto-categoria di appartenenza
- localizzazione geografica
- descrizione
- prezzo
- stato dell'oggetto (menù a tendina con 4 categorie: nuovo, usato ma in perfette condizioni, qualche lieve difetto, difettoso)
- una o più foto
- informazioni del proprietario (email, numero di telefono)
- disponibilità a spedire (switch "si/no")

L'amministratore ha pieno accesso a tutte le informazioni e puo' modificare l'organizzazione delle categorie e cancella/modificare ogni informazione di ogni singolo oggetto.

L'utente

- può registrarsi alla piattaforma
- può creare un annuncio, modificarlo, cancellarlo
- chiedere informazioni su un altro annuncio tramite chat
- fare ricerche per nome dell'oggetto e/o localizzazione (entro x km (dove x e' specificato dall'utente) da dove si trova rispetto alla localizzazione dell'oggetto) e/o prezzo (fissando un range o un limite superiore/inferiore) e/o disponibilità a spedire
- salvare delle ricerche con uno o piu' criteri e ricevere notifiche quando un oggetto è stato aggiunto con le caratteristiche della ricerca (notifica solo all'avvio dell'app).

Il sistema deve anche prevedere la chiusura dell'accordo tra due utenti, segnalando che l'oggetto e' stato venduto.
Il venditore setta semplicemente che l'oggetto è stato venduto.

Quanto sopra porta ad una valutazione massima di 25/30.

Per puntare ad voto più alto occorre aggiungere le seguenti funzionalità:

Amministratore:

- Deve poter chattare con gli utenti
- Deve poter eliminare utenti o spenderli dalle attività
- Deve poter avere accesso a statistiche quali: oggetti in vendita in totale, oggetti in vendita per specifico utente, tempo medio (in giorni) da quando un oggetto compare nella piattaforma e viene venduto sul totale degli oggetti venduti in passato, classifica utenti con voto più alto, numero di oggetti in vendita per un certo raggio di distanza da un punto specificato dall'amministratore.

Utente:

- ogni utente ha un budget e ogni volta che acquista o vende un oggetto, questo budget sale o scende. Per aggiugere fondi al budget, simulare l'inserimento dei dati di una carta di credito.
- può aggiungere uno o più oggetti nella sua lista dei preferiti. Ogni volta che l'oggetto subisce una variazione al prezzo, l'utente viene notificato.
- concluso l'affare, sia che sia acquirente o venditore, deve poter dare un voto da 1 a 5 all'altro utente e lasciare una breve recensione.
## Feature Implementate
Sono state implementate tutte le feature richieste.
Per visualizzare in dettaglio come sono stati interpretati i requisiti, si rimanda al file README.md (presente nella root del progetto).
## Scelte Implementative
L'applicazione è indicata all'utilizzo per un dispositivo che supporti l'API 33; inoltre è stata testata principalmente su Google Pixel 4.

L'applicazione utilizza una sola Activity, su cui vengono aggiunti i vari Fragment.
Sono stati utilizzati gli asset vettoriali forniti da Android, in modo tale da essere in linea con lo stile del sistema operativo.

Non è possibile registrarsi come Admin, poiché questa possibililtà fornirebbe funzionalità troppo importanti per un utente qualunque.
Per questo motivo, un account può diventare Admin solo se un altro Admin gliene fornisce la possibilità (direttamente da Firestore).

La modifica di una categoria da parte di un Admin è stata implementata ma non utilizzata all'interno dell'applicazione, per evitare inconsistenze per i prodotti già esistenti.
Lo stesso vale per la modifica delle sotto-categorie.
## Spiegazione della Navigazione
1. All'avvio dell'applicazione, viene presentata la pagina di login / di registrazione.
2. Una volta effettuato il log in o il sign up, viene mostrata la pagina di ricerca che presenta una NavBar al fondo.
Questa barra di navigazione permette di spostarsi facilmente tra le pagine dell'applicazione; in particolare le pagine della NavBar sono le seguenti, da sinistra a destra:
> - PersonalPage
> - MyProducts
> - MyChats
> - SearchPage
3. La PersonalPage mostra la propria mail, i prodotti preferiti ed il budget.
Da questa pagina è possibile aggiungere credito, effettuare il sign out e cancellare il proprio Account.
> - Nel caso in cui si sia Admin, questa pagina è rimpiazzata dall'AdminPanel, in cui si vedono le statistiche (come da richieste); è inoltre possibile cercare un utente (e in seguito bloccarlo o eliminarlo) o creare nuove categorie/sotto-categorie.
4. La pagina MyProducts permette di creare un nuovo prodotto e vedere i propri prodotti in vendita.
5. Nella pagina MyChats sono mostrate tutte le chat attive.
Queste chat sono cliccabili e permettono la visualizzazione dei messaggi scambiati in passato.
6. Da SearchPage è possibile interrogare il database con vari filtri (come da richieste) e visualizzare il risultato della ricerca.

Dalla pagina di dettaglio del prodotto, è possibile avviare una chat con l'altro utente.
Se si è il creatore o l'Admin, la modifica e la cancellazione del prodotto vengono rese disponibili.

## Commenti a parti di codice significativi
#### Immagini
La gestione delle immagini viene fatta grazie alla libreria Glide* e allo storage di firebase.
Quando l'utente inserisce un'immagine, viene aperta una galleria personalizzata dalla quale si possono selezionare fino a 3 immagini alla volta. 
Per ogni immagine scelta viene creato un oggetto Photo che contiene un id e un indirizzo URI.
Terminato l'inserimento del prodotto, al click finale per l'aggiunta, le immagini verranno caricate sullo storage in una sotto cartella, identificata con l'id del prodotto.
Dalla pagina di visualizzazione del prodotto sarà necessario ottenere un URL per il download, tramite una chiamata allo storage, che passato a Glide caricherà le immagini nella view.

*=Glide è una libreria che permette il caricamento efficente e asincrono delle immagini
#### Notifiche
Quando l'utente effettua il login, vengono creati automaticamente gli SnapshotListener per le sue ricerche salvate, i prodotti preferiti e le chat attive.
In questo modo, l'utente può ricevere notifiche per ogni aggiornamento di ciò che a lui interessa.

Tra questi tre punti d'interesse, quella che si è rivelato più complicato è sicuramente la notifica di nuovi messaggi nella chat.

Per riuscire nell'intento, è stato necessario salvare su Firestore il numero di messaggi inviati nella chat e l'ultimo messaggio scambiato.
Il primo parametro serve per evitare di inviare notifiche riguardanti messaggi passati e già visualizzati.
Il secondo viene usato dallo SnapshotListener per l'invio della notifica, nel caso in cui il messaggio in questione non sia stato inviato dall'utente correntemente loggato.

Questo SnapshotListener viene usato quando l'utente inizia una chat a partire da un prodotto.
Nel caso in cui all'apertura dell'applicazione esistano già chat attive, ci si avvale di un ChildEventListener che viene aggiunto al RealtimeDatabase (in cui vengono persistiti i messaggi).
Questo ChildEventListener osserva l'aggiunta di nuovi nodi ad una chat specifica e, se il messaggio arriva dall'altra persona, notifica l'utente.

#### NavGraph
L'applicazione si avvale del NavGraph per la navigazione.
Questo è un grafo in cui i nodi sono i Fragment disponibili e gli archi rappresentano le direzioni disponibili da un dato nodo.
L'utilizzo del NavGraph semplifica sia lo spostamento tra i Fragment che il passaggio di variabili.

Questo strumento permette anche di effettuare il pop dello stack delle pagine passate, dopo aver creato la nuova pagina da mostrare all'utente.
Per questo motivo una volta effettuato il login o il sign up, non è possibile ritornarvici.

#### NavBar
La NavBar permette una navigazione più fluida dell'applicazione suddividendola in categorie, più facili da gestire.
Fa riferimento agli id del NavGraph, grazie ai quali può immediatamente richiamare il fragment richiesto.
Essa viene inserita una volta sola dala MainActivity e poi fatta scomparire se superflua.

#### Callout e Snapshot
Abbiamo raggruppato le chiamate a Firestore nella classe Callout, che espone dei metodi statici.
L'aggiornamento delle View all'interno del Fragment attualmente attivo avviene grazie all'uso di funzioni callback da parte di questi metodi statici.
Nel caso in cui qualcosa dovesse andare storto, il metodo usato mostra un Toast message con un messaggio significativo; altrimenti, se l'operazione è andata a buon fine, viene chiamata la callback.

Questo stesso ragionamento è stato utilizzato anche per i metodi della classe Snapshot, in cui vengono principalmente creati e aggiornati gli SnapshotListener.

## Utenti
| Email | Password | Dettagli |
| - | - | - |
| canonico@uniupo.it | gYLLop68+1% | Ha un prodotto tra i preferiti |
| easyuser@gmail.com | easyuser1! | ha 3 prodotti in vendita |
| admin@reuse.it | adminreuse1! | &Egrave; un admin |
## Progetti iOS
- Milanesio Fabio: /iOS/MilanesioFabio.zip
- Chissotti Alfredo: /iOS/ChissottiAlfredo.zip
<!-- per trasformare questo file in pdf: https://md2pdf.netlify.app/ -->
